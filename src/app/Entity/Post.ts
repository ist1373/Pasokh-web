import {UploadedFile} from "./UploadedFile";
import {Category} from "./Category";
/**
 * Created by iman on 5/22/2018.
 */
export class Post {
  id: number;

  creationDate: Date;

  publicationDate: Date;

  title: string;

  content: string;

  coverImage: UploadedFile;

  category : Category;

  constructor() {
  }
}
