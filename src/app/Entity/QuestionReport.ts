import {User} from "./User/User";
import {UploadedFile} from "./UploadedFile";
import {ChatRoom} from "./ChatRoom";
import {Question} from "./Question";
/**
 * Created by iman on 5/22/2018.
 */
export class QuestionReport{
  constructor()
  {

  }

  id: number;

  uid: string;


  tutor: User;

  creationDate: Date;

  description: String;

  question: Question;

  reportType: ReportType;
}



export enum ReportType {
  ManyQuestion = 'ManyQuestion', NotQuestion = 'NotQuestion', IrrevelentQuestion = 'IrrevelentQuestion', Other = 'Other'
}
