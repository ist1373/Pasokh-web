import {User} from "./User/User";
/**
 * Created by iman on 5/22/2018.
 */
export class InviteToken{
   id: number;


  token: string;


    creationDate: Date;


    caller: User;


   invited: User;

}
