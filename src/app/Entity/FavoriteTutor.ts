import {User} from "./User/User";
import {Tutor} from "./User/Tutor";
/**
 * Created by iman on 5/22/2018.
 */
export class FavoriteTutor {
  id: number;

  questioner: User;

  creationDate: Date;

  tutors: Tutor;
}
