import {User} from "./User/User";
/**
 * Created by iman on 5/22/2018.
 */
export class ContactMessage {
  id: number;

  email: string;

  content: string;

  createdBy: User;


  deviceId: string;


  creationDate: Date;
}
