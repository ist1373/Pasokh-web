import {User} from "./User/User";
/**
 * Created by iman on 5/22/2018.
 */
export class Transaction {

  id: number;


  uid: string;


  amount: number;


  description: string;


  refId: string;


  authority: string;


  errorCode: number;


  creationDate: Date;


  payer: User;

  paymentType: PaymentType;

  paymentState: PaymentState;

}

export enum PaymentType {Content}
export enum PaymentState {Done, Cancel, Error, Pending, Fraud, Archived}


