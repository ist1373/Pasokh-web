import {UploadedFile} from "./UploadedFile";
/**
 * Created by iman on 5/22/2018.
 */
export class Category {
  id: number;

  title: string;


  coverImage: UploadedFile;


  childCount: number;

  categoryIndex: number;
}
