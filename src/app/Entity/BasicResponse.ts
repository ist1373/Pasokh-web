/**
 * Created by iman on 12/4/2017.
 */

export class BasicResponse {

  code: number;

  message: string;

  success: boolean;

  data: object;


}
