import {User} from "./User/User";
import {ChatRoom} from "./ChatRoom";
import {UploadedFile} from "./UploadedFile";
/**
 * Created by iman on 5/22/2018.
 */

export class ChatMessage {

  id: number;

  uid: string;

  authorUser: User;

  recipientUser: User;


  timeSent: Date;

  contents: string;

  mediaFile: UploadedFile


  chatRoom: ChatRoom;

  messageType: MessageType;

}


export enum MessageType {
  SEND_TEXT = 'SEND_TEXT',
  SEND_IMAGE = 'SEND_IMAGE',
}
