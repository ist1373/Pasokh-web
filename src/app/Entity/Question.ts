import {User} from "./User/User";
import {UploadedFile} from "./UploadedFile";
import {ChatRoom} from "./ChatRoom";
/**
 * Created by iman on 5/22/2018.
 */
export class Question{
  constructor()
  {

  }

  id: number;

  uid: string;


  questioner: User;

  responder: User;


  creationDate: Date;

  endDate: Date ;

  acceptDate: Date ;

  rejectedDate: Date ;

  description: String;


  image: UploadedFile;

  rate:number

  credit:number

  grade: Grade

  chatRoom: ChatRoom;

  questionState: QuestionState;

  questionTopic: QuestionTopic;
}



export enum Grade {
  Seventh, Eighth, Ninth, Tenth, Eleventh, Twelfth
}

export enum QuestionState {
  Pending = 'Pending', Accepted = 'Accepted', Finished = 'Finished', Rejected = 'Rejected' , WaitingForTutor = 'WaitingForTutor' , RejectedByTutor = 'RejectedByTutor'
}

export enum QuestionTopic {
  Physics, Chemistry, Biology, Literature, Math, Arabic, Religious, English
}

export enum ReportTopic {
  NotAnswered = 'NotAnswered', WrongAnswered = 'WrongAnswered', LateAnswered = 'LateAnswered', Other = 'Other'
}
