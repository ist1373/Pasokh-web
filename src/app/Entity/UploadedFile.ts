import {User} from "./user/User";
import {serializeType} from "../app.globals";

/**
 * Created by iman on 12/4/2017.
 */


export class UploadedFile {

  id: number;

  name: string;

  size: number;

  originalPath: string;

  thumbnailPath: string;

  description: string;

  creationDate: Date;

  fileType: string;


  createdBy: User;

  isTransferred: boolean;


  status: number = 400;

  accessType: AccessType;
}

export enum AccessType {Public = 'Public', Private = 'Private', AccessOnPay = 'AccessOnPay',Forbidden = 'Forbidden'}
