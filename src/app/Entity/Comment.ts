import {User} from "./User/User";
import {Post} from "./Post";
/**
 * Created by iman on 5/22/2018.
 */
export class Comment {

  id: number;

  rate: number;

  description: string;

  createdBy: User;

  post: Post;

  creationDate: Date;

  commentState: CommentState;

}

export enum CommentState {Pending, Published, Canceled}
