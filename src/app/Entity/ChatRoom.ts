import {User} from "./User/User";
/**
 * Created by iman on 5/22/2018.
 */

export class ChatRoom {

  id: number;

  uid: string;

  questioner: User;

  responder: User;

  creationDate: Date;

}
