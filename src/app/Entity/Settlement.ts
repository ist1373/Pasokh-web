import {User} from "./User/User";
/**
 * Created by iman on 5/22/2018.
 */
export class Settlement {

    id: number;


    uid: string;


    amount: number;


    refId: string;


    creditNumber: string;


    creationDate: Date;


    receiver: User;


    settlementState: SettlementState;



  settlementType: SettlementType;


}

export enum SettlementState {Done, Cancel, Error, Pending}
export enum SettlementType {Content}
