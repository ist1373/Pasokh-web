import {UploadedFile} from "../UploadedFile";
/**
 * Created by iman on 5/22/2018.
 */
export class User {
  id: number;

  uid: string;

  username: string;

  email: string;


  mobileNumber: string;


  password: string;

  firstName: string;

  lastName: string;

  isPresent: boolean;


  playerId: string;

  lastPasswordResetDate: string;

  gender: Gender;

  profileImage: UploadedFile;

  userState: UserState;

}

export enum Gender {
  Male = 'Male', Female = 'Female'
}
export enum UserState {
  Pending = 'Pending', Verified = 'Verified' , Qualified = 'Qualified' , Rejected = 'Rejected' , Banned = 'Banned' , NeedToEdit = 'NeedToEdit' , Deleted = 'Deleted'
}
