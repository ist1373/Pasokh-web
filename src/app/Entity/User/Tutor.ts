import {User} from "./User";
import {UploadedFile} from "../UploadedFile";
/**
 * Created by iman on 5/22/2018.
 */
export class Tutor extends User {
  deviceId: string;

  systemOS: string;

  systemVersion: string;

  systemDevice: string;

  shabaCode: string;

  biography: string;

  nationalCode: string;

  postalCode: string;

  address: string;

  avgStar: number;

  nationalCardImage: UploadedFile;

  topics: Array<String> = [];
}
