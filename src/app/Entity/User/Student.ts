import {User} from "./User";
import {City} from "../City";
import {GradeSelComponent} from "../../Components/sign-in-up/grade-sel/grade-sel.component";
import {Grade} from "../Question";
/**
 * Created by iman on 5/22/2018.
 */

export class Student extends User {

  deviceId: string;

  systemOS: string;

  systemVersion: string;

  systemDevice: string;

  inviteToken: string;

  grade: Grade;

  city: City;

  field:Field;

  school:string;

  average: string;

  credit:number
}

export enum Field {
  Math, NaturalScience, Humanities, Technical
}
