import {User} from "./User/User";
import {Question} from "./Question";
/**
 * Created by iman on 7/14/2018.
 */
export class EndedQuestion
{
  id: number;

  uid: string;


  credit: number;

  bid: number;


  creationDate: Date;

  responder: User ;

  question: Question ;

  rejectedDate: Date ;

  description: String;

  endedQuestionState: EndedQuestionState;


}
export enum EndedQuestionState {Completed = 'Completed', Archived = 'Archived'}
