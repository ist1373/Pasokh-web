/**
 * Created by iman on 5/21/2018.
 */
export class TutorDto {
  id: number;
  username: string;
  password: string;
  email: string;
  mobileNumber: string;
  playerId: string;
  deviceId: string;
  systemOS: string;
  systemVersion: string;
  systemDevice: string;
  shabaCode: string;
  nationalCode: string;
}
