import {UploadedFile} from "./UploadedFile";
/**
 * Created by iman on 5/22/2018.
 */
export class Purchase {
  id: number;

  title: string;

  description: string;

  price: number;

  creationDate: Date;

  image: UploadedFile;

  isShow:boolean

  credit:number;


}
