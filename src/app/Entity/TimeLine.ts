/**
 * Created by iman on 10/02/2017.
 */
import {Injectable} from "@angular/core";
import {Province} from "./Province";
import {User} from "./User/User";
import {QuestionTopic} from "./Question";


export class TimeLine {

    id: number;

    creationDate: Date;

    responder: User;

    star: number;

    eventType: EventType;

    questionTopic: QuestionTopic;

}

export enum EventType {
  NewQuestion, NewAnswer, TakenStar
}
