import {Question, ReportTopic} from "./Question";
import {User} from "./User/User";
/**
 * Created by iman on 5/22/2018.
 */
export class ComplaintReport{
  id: number;

  creationDate: Date;

  description: string;

  question: Question;

  user: User;

  reportTopic: ReportTopic;

  reportState: ReportState;

}
export enum ReportState {Seen, notSeen}
