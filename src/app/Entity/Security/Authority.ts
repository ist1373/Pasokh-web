/**
 * Created by iman on 5/22/2017.
 */

export class Authority {
  id: number;
  authority: AuthorityName;
}

export enum AuthorityName {
  ROLE_EMPLOYEE, ROLE_ADMIN, ROLE_STUDENT, ROLE_TUTOR
}
