import {Authority} from "./Authority";
/**
 * Created by iman on 5/22/2017.
 */

export class JwtAuthenticationResponse {
  token: string;
  authorities: Array<Authority>;
}
