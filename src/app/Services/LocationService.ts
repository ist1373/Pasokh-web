import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {Province} from "../Entity/Province";
/**
 * Created by iman on 6/21/2018.
 */

@Injectable()
export class LocationService {

  constructor(private http: HttpClient) { }

  getProvinces(){
    return this.http.get<Array<Province>>(AppContextUrl + 'location/province')
  }

  getCities(provinceId: number){
    return this.http.get<Array<Province>>(AppContextUrl + 'location/province/' + provinceId + '/city')
  }


}
