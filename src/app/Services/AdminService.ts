import { Injectable } from '@angular/core';
import {Headers, Http,RequestOptions} from "@angular/http";
import {Subject} from "rxjs/Subject";
import {JwtAuthenticationRequest} from "../Entity/Security/JwtAuthenticationRequest";
import {map} from "rxjs/operators";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {StudentDto} from "../Entity/DTO/StudentDto";
import {TutorDto} from "../Entity/DTO/TutorDto";
import {Tutor} from "../Entity/User/Tutor";
import {CacheService} from "ng2-cache-service";
import {User} from "../Entity/User/User";
import {Admin} from "../Entity/User/Admin";
import {BasicResponse} from "../Entity/BasicResponse";



@Injectable()
export class AdminService {

  constructor(private http: HttpClient) { }
  getAllAdmins(page){
    return this.http.get<Array<Admin>>(AppContextUrl + 'user/admin?page=' + page);
  }

  getAdminByID(id) {
    return this.http.get<Admin>(AppContextUrl + 'user/admin/' + id);
  }

  registerAdmin(admin: Admin) {
    return this.http.post<BasicResponse>(AppContextUrl + 'user/admin' , admin);
  }

}
