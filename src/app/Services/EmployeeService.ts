import { Injectable } from '@angular/core';
import {Headers, Http,RequestOptions} from "@angular/http";
import {Subject} from "rxjs/Subject";
import {JwtAuthenticationRequest} from "../Entity/Security/JwtAuthenticationRequest";
import {map} from "rxjs/operators";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {StudentDto} from "../Entity/DTO/StudentDto";
import {TutorDto} from "../Entity/DTO/TutorDto";
import {Tutor} from "../Entity/User/Tutor";
import {CacheService} from "ng2-cache-service";
import {User} from "../Entity/User/User";
import {Employee} from "../Entity/User/Employee";
import {BasicResponse} from "../Entity/BasicResponse";



@Injectable()
export class EmployeeService {

  constructor(private http: HttpClient) { }
  getAllEmployees(page){
    return this.http.get<Array<Employee>>(AppContextUrl + 'user/employee?page=' + page);
  }

  getEmployeeByID(id) {
    return this.http.get<Employee>(AppContextUrl + 'user/employee/' + id);
  }

  registerEmployee(employee: Employee) {
    return this.http.post<BasicResponse>(AppContextUrl + 'user/employee' , employee);
  }

  editEmployee(id, employee: Employee) {
    return this.http.put(AppContextUrl + 'user/employee/' + id, employee);
  }

}
