import { Injectable } from '@angular/core';
import {Headers, Http,RequestOptions} from "@angular/http";
import {Subject} from "rxjs/Subject";
import {JwtAuthenticationRequest} from "../Entity/Security/JwtAuthenticationRequest";
import {map} from "rxjs/operators";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";

import {BasicResponse} from "../Entity/BasicResponse";
import {Purchase} from "../Entity/Purchase";



@Injectable()
export class PurchaseService {


  constructor(private http: HttpClient) {

  }

  getAvailablePurchase()
  {
    return this.http.get<Array<Purchase>>(AppContextUrl + 'purchase/available');
  }
  getAllPurchase()
  {
    return this.http.get<Array<Purchase>>(AppContextUrl + 'purchase');
  }

  editPurchase(p: Purchase)
  {
    return this.http.put<BasicResponse>(AppContextUrl + 'purchase/' + p.id, p);
  }

  addPurchase(p: Purchase)
  {
    return this.http.post<BasicResponse>(AppContextUrl + 'purchase/', p);
  }


}
