import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {Province} from "../Entity/Province";
import {BasicResponse} from "../Entity/BasicResponse";
/**
 * Created by iman on 6/21/2018.
 */

@Injectable()
export class BidService {

  constructor(private http: HttpClient) { }

  addBid(qId, amount) {
    return this.http.post<BasicResponse>(AppContextUrl + 'bid?quesId=' + qId + '&amount=' + amount, null);
  }

  addDirectBid(qid , accept)
  {
    return this.http.post<BasicResponse>(AppContextUrl + 'bid?quesId=' + qid + '&accept=' + accept, null);
  }


}
