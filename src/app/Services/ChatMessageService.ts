/**
 * Created by iman on 7/7/2018.
 */
import { Injectable } from '@angular/core';
import {Headers, Http,RequestOptions} from "@angular/http";
import {Subject} from "rxjs/Subject";
import {JwtAuthenticationRequest} from "../Entity/Security/JwtAuthenticationRequest";
import {map} from "rxjs/operators";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {StudentDto} from "../Entity/DTO/StudentDto";
import {Question} from "../Entity/Question";
import {CacheService} from "ng2-cache-service";
import {User} from "../Entity/User/User";
import {ChatMessage} from "../Entity/ChatMessage";



@Injectable()
export class ChatMessageService {

  constructor(private http: HttpClient,private cacheService:CacheService) {

  }


  addChatMessage(chatMessage: ChatMessage, roomUid: string) {
    return this.http.post(AppContextUrl + 'chat/room/' + roomUid, chatMessage);
  }


  getExistingChatMessages(roomUid: string, page: number){
    let params = new HttpParams();

    params = params.append('page', page + '');



    return this.http.get<Array<ChatMessage>>(AppContextUrl + 'chat/room/' + roomUid , {params: params});
  }



}
