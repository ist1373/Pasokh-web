import { Injectable } from '@angular/core';
import {Headers, Http,RequestOptions} from "@angular/http";
import {Subject} from "rxjs/Subject";
import {JwtAuthenticationRequest} from "../Entity/Security/JwtAuthenticationRequest";
import {map} from "rxjs/operators";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {User} from "../Entity/User/User";



@Injectable()
export class UserService {


  constructor(private http: HttpClient) {

  }

  public static tokenHeader = 'Authorization';

  login(username: string, password: string) {

    var body = new JwtAuthenticationRequest();
    body.username = username;
    body.password = password;
    return this.http.post(AppContextUrl + 'user/auth' , body);
  }

  registerVerification(code: string) {
    return this.http.get(AppContextUrl + 'user/registrationConfirm?token=' + code);
  }

  forgotPass(empho: string) {
    return this.http.post(AppContextUrl + 'user/forgotPass?username=' + empho, null);
  }

  getMyProfile()
  {
    return this.http.get(AppContextUrl + 'user');
  }

  findUser(email, phone, page)
  {
    let params = new HttpParams();
    params = params.append('email', email);
    params = params.append('phone', phone);
    params = params.append('page', page);
    return this.http.get<Array<User>>(AppContextUrl + 'user/find', {params: params});
  }


  // getUser()
  // {
  //   return this.http.get(AppContextUrl + 'user',this.setAuthorizationHeader()).pipe(
  //     map(res => plainToClass<User, Object>(User, res.json()))
  //   )
  // }
  //
  // registerationConfirm(token:string)
  // {
  //   return this.http.get(AppContextUrl + 'user/registrationConfirm?token=' + token);
  // }
  //
  // getUsersByUserName(username)
  // {
  //   return this.http.get(AppContextUrl + 'user?username=' + username ,this.setAuthorizationHeader()).map(res =>{
  //     var bookMarket = plainToClass<User, Object[]>(User, res.json());
  //     return bookMarket;
  //   });
  // }
  //
  // // registerFinalUser(user:Student)
  // // {
  // //   return this.http.post(AppContextUrl + 'user/foreign-student',user);
  // // }
  //
  //
  // setAuthorizationHeader()
  // {
  //   let headers = new Headers({});
  //   headers.set('Authorization',this.cacheService.get('token'));
  //   let options = new RequestOptions({ headers: headers});
  //   return options;
  // }
  //
  // editUser(user:User)
  // {
  //   return this.http.put(AppContextUrl+'user/foreign-student',user,this.setAuthorizationHeader());
  // }


}
