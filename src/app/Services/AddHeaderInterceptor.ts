import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import {Observable} from "rxjs/Rx";
import {CacheService} from "ng2-cache-service";
import {Injectable} from "@angular/core";

@Injectable()
export class AddHeaderInterceptor implements HttpInterceptor {
  constructor(private cacheService: CacheService){

  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    console.log(req)
    if(this.cacheService.exists('userToken'))
    {
      const clonedRequest = req.clone({ headers: req.headers.set('Authorization', this.cacheService.get('userToken')) });
      return next.handle(clonedRequest);
    }
    else
    {
      return next.handle(req);
    }

  }
}
