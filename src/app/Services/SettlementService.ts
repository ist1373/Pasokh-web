import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {Province} from "../Entity/Province";
import {BasicResponse} from "../Entity/BasicResponse";
import {CacheService} from "ng2-cache-service";
import {User} from "../Entity/User/User";
import {Settlement} from "../Entity/Settlement";
/**
 * Created by iman on 6/21/2018.
 */

@Injectable()
export class SettlementService {

  constructor(private cacheService: CacheService, private http: HttpClient) { }

  getAvailableMoney() {
    console.log('dddd')
    let user = this.cacheService.get('user') as User;
    return this.http.get<BasicResponse>(AppContextUrl + 'settlement/cash?userId=' + user.id);
  }

  addSettlement() {
    return this.http.post<BasicResponse>(AppContextUrl + 'settlement', null);
  }

  getSettlementsByTutor(page) {
    console.log('dddd')
    let user = this.cacheService.get('user') as User;
    return this.http.get<Array<Settlement>>(AppContextUrl + 'settlement/tutor/' + user.id + '?page=' + page);
  }
}
