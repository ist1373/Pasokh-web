/**
 * Created by iman on 12/8/2017.
 */
import {Injectable} from "@angular/core";
import {CacheService} from "ng2-cache-service";
import {HttpClient, HttpHeaderResponse, HttpHeaders, HttpParams} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {AccessType} from "../Entity/UploadedFile";


@Injectable()
export class FileService {
  constructor(private http: HttpClient, private cacheService: CacheService) {
  }


  uploadFile(file: File, accessType: string , desc:string) {
    let uploadUrl = AppContextUrl + 'file/upload';
    let formData: FormData = new FormData();
    if (file.name) {
      formData.append('file', file, file.name);
    }
    else {
      formData.append('file', file, '1.jpg');
    }
    formData.append('accessType', accessType.toString() );
    formData.append('desc', desc.toString() );
    return this.http.post(uploadUrl, formData);
  }


  //
  // registerStudentByEmployee(studentType:String,passportNo?:String,studentNo?:String,saorgNo?:String) {
  //   let params = new HttpParams();
  //   if (studentNo)
  //   {
  //     params = params.append('studentNo', studentNo.toString());
  //   }
  //   if (passportNo)
  //   {
  //     params = params.append('passportNo', passportNo.toString());
  //   }
  //   if (saorgNo)
  //   {
  //     params = params.append('saorgNo', saorgNo.toString());
  //   }
  //
  //   params = params.append('studentType', studentType.toString());
  //   return this.http.post(AppContextUrl + 'student/add', '' , {params: params,headers:new HttpHeaders().set('Authorization',this.cacheService.get('userToken'))});
  // }
  //
  //
  // loginStudent(captcha:String,passportNo?:String ,studentNo?:String,saorgNo?:String) {
  //   let params = new HttpParams();
  //   if (studentNo)
  //   {
  //     params = params.append('studentNo', studentNo.toString());
  //   }
  //   if (passportNo)
  //   {
  //     params = params.append('passportNo', passportNo.toString());
  //   }
  //   if (saorgNo)
  //   {
  //     params = params.append('saorgNo', saorgNo.toString());
  //   }
  //
  //   params = params.append('captcha', captcha.toString());
  //
  //
  //   return this.http.post(AppContextUrl + 'student/register', '' , {params: params});
  // }
  //
  //
  // registerForeignStudent(foreignStudent:ForeignStudent) {
  //   return this.http.post(AppContextUrl + 'student/register/foreignStudent', foreignStudent);
  // }
  //
  // registerLanguageLearner(languageLearner:LanguageLearner) {
  //   return this.http.post(AppContextUrl + 'student/register/languageLearner', languageLearner);
  // }
  //
  // getForeignStudentByID(id:number)
  // {
  //   return this.http.get<ForeignStudent>(AppContextUrl + 'student/foreignStudent/' + id, {headers:new HttpHeaders().set('Authorization',this.cacheService.get('token'))});
  // }
  //
  // getStudents(page:number,passportNo?:String ,studentNo?:String,saorgNo?:String)
  // {
  //   let params = new HttpParams();
  //   if (studentNo)
  //   {
  //     params = params.append('studentNo', studentNo.toString());
  //   }
  //   if (passportNo)
  //   {
  //     params = params.append('passportNo', passportNo.toString());
  //   }
  //   if (saorgNo)
  //   {
  //     params = params.append('saorgNo', saorgNo.toString());
  //   }
  //   params = params.append('page', page.toString());
  //
  //   return this.http.get<Array<Student>>(AppContextUrl + 'student',{params:params})
  //
  //
  // }
  //
  //
  //
  // getUser() {
  //   return this.http.get(AppContextUrl + 'user',{headers:new HttpHeaders().set('Authorization',this.cacheService.get('token'))}).pipe(
  //     map(res => plainToClass<User, Object>(User, res['json']))
  //   )
  // }
  //
  // registerationConfirm(token: string) {
  //   return this.http.get(AppContextUrl + 'user/registrationConfirm?token=' + token);
  // }



}
