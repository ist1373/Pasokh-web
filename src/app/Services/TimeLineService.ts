import { Injectable } from '@angular/core';
import {Headers, Http,RequestOptions} from "@angular/http";
import {Subject} from "rxjs/Subject";
import {JwtAuthenticationRequest} from "../Entity/Security/JwtAuthenticationRequest";
import {map} from "rxjs/operators";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {StudentDto} from "../Entity/DTO/StudentDto";
import {Question} from "../Entity/Question";
import {CacheService} from "ng2-cache-service";
import {User} from "../Entity/User/User";
import {TimeLine} from "../Entity/TimeLine";



@Injectable()
export class TimeLineService {

  constructor(private http: HttpClient,private cacheService:CacheService) {

  }



  getTimeLines(page: number, size: number){
    // let user = this.cacheService.get('user') as User;
    let params = new HttpParams();
    //
    params = params.append('page', page + '');
    //
    // params = params.append('size', size + '');

    return this.http.get<Array<TimeLine>>(AppContextUrl + 'timeline' , {params: params});
    // return this.http.get<Array<TimeLine>>(AppContextUrl + 'timeline');
  }


  // login(username: string, password: string) {
  //   // let params = new HttpParams();
  //   // params = params.append('captcha', captcha.toString());
  //
  //   var body = new JwtAuthenticationRequest();
  //   body.username = username;
  //   body.password = password;
  //
  //   return this.http.post(AppContextUrl + 'user/auth' , body);
  // }



  // getUser()
  // {
  //   return this.http.get(AppContextUrl + 'user',this.setAuthorizationHeader()).pipe(
  //     map(res => plainToClass<User, Object>(User, res.json()))
  //   )
  // }
  //
  // registerationConfirm(token:string)
  // {
  //   return this.http.get(AppContextUrl + 'user/registrationConfirm?token=' + token);
  // }
  //
  // getUsersByUserName(username)
  // {
  //   return this.http.get(AppContextUrl + 'user?username=' + username ,this.setAuthorizationHeader()).map(res =>{
  //     var bookMarket = plainToClass<User, Object[]>(User, res.json());
  //     return bookMarket;
  //   });
  // }
  //
  // // registerFinalUser(user:Student)
  // // {
  // //   return this.http.post(AppContextUrl + 'user/foreign-student',user);
  // // }
  //
  //
  // setAuthorizationHeader()
  // {
  //   let headers = new Headers({});
  //   headers.set('Authorization',this.cacheService.get('token'));
  //   let options = new RequestOptions({ headers: headers});
  //   return options;
  // }
  //
  // editUser(user:User)
  // {
  //   return this.http.put(AppContextUrl+'user/foreign-student',user,this.setAuthorizationHeader());
  // }


}
