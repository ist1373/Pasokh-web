import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {Province} from "../Entity/Province";
import {BasicResponse} from "../Entity/BasicResponse";
import {Category} from "../Entity/Category";
/**
 * Created by iman on 6/21/2018.
 */

@Injectable()
export class CategoryService {

  constructor(private http: HttpClient) { }

  getCategories() {
    return this.http.get<Array<Category>>(AppContextUrl + 'category');
  }

  addCategory(cat: Category) {
    return this.http.post<BasicResponse>(AppContextUrl + 'category', cat);
  }

}
