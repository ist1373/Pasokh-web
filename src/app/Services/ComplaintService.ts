import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {Province} from "../Entity/Province";
import {BasicResponse} from "../Entity/BasicResponse";
import {ComplaintReport} from "../Entity/ComplaintReport";
/**
 * Created by iman on 6/21/2018.
 */

@Injectable()
export class ComplaintService {

  constructor(private http: HttpClient) { }

  addComplaintReport(qId, desc, topic) {
    return this.http.post<BasicResponse>(AppContextUrl + 'complaintReport?quesId=' + qId + '&desc=' + desc + '&topic=' + topic, null);
  }

  getReportByTopic(topic , page) {
    let params = new HttpParams();
    params = params.append('page', page + '');
    if (topic != null)
    {
      params = params.append('topic', topic + '');
    }
    return this.http.get<Array<ComplaintReport>>(AppContextUrl + 'complaintReport', {params: params});
  }


}
