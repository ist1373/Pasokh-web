import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {Province} from "../Entity/Province";
import {BasicResponse} from "../Entity/BasicResponse";
import {QuestionReport} from "../Entity/QuestionReport";
/**
 * Created by iman on 6/21/2018.
 */

@Injectable()
export class QuestionReportService {

  constructor(private http: HttpClient) { }

  addReport(qId, desc, type) {
    return this.http.post<BasicResponse>(AppContextUrl + 'questionReport?quesId=' + qId + '&desc=' + desc + '&reportType=' + type, null);
  }

  getQuestionReports(reportType , page) {
    let params = new HttpParams();
    params = params.append('page', page + '');

    if (reportType != null)
    {
      params = params.append('reportType', reportType + '');
    }
    return this.http.get<Array<QuestionReport>>(AppContextUrl + 'questionReport', {params: params});
  }

}
