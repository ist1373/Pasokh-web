import { Injectable } from '@angular/core';
import {Headers, Http,RequestOptions} from "@angular/http";
import {Subject} from "rxjs/Subject";
import {JwtAuthenticationRequest} from "../Entity/Security/JwtAuthenticationRequest";
import {map} from "rxjs/operators";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AngularClientUrl, AppContextUrl} from "../app.globals";

import {BasicResponse} from "../Entity/BasicResponse";
import {Purchase} from "../Entity/Purchase";
import {CacheService} from "ng2-cache-service";
import {User} from "../Entity/User/User";
import {Transaction} from "../Entity/Transaction";



@Injectable()
export class TransactionService {


  constructor(private http: HttpClient, private cacheService: CacheService) {

  }

  zarrinPaymentRequest(purchase: Purchase)
  {
  console.log(purchase)
    // let params = new HttpParams();
    // params = params.append('pId', purchase.id + '');
    // params = params.append('callbackUrl', AngularClientUrl + 'student/purchasePage');
    return this.http.post<BasicResponse>(AppContextUrl + 'transaction/paymentRequestZarin?pId=' + purchase.id + '&amount=' + purchase.price +  '&callbackUrl=' +  AngularClientUrl + 'student/purchasePage?pId=' + purchase.id, null);
  }


  zarrinPaymentVerification(pid: number , authority:string)
  {
    let user = this.cacheService.get('user') as User;
    return this.http.post<BasicResponse>(AppContextUrl + 'transaction/verifyPaymentZarin?pId=' + pid  + '&userId=' + user.id + '&authority=' + authority, null);
  }

  getTransactionByPayer(page)
  {
    let user = this.cacheService.get('user') as User;
    let params = new HttpParams();

    params = params.append('page', page + '');

    return this.http.get<Array<Transaction>>(AppContextUrl + 'transaction/user/' + user.id + '/done',{params: params});
  }
}
