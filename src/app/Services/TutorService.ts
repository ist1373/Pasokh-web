import { Injectable } from '@angular/core';
import {Headers, Http,RequestOptions} from "@angular/http";
import {Subject} from "rxjs/Subject";
import {JwtAuthenticationRequest} from "../Entity/Security/JwtAuthenticationRequest";
import {map} from "rxjs/operators";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {StudentDto} from "../Entity/DTO/StudentDto";
import {TutorDto} from "../Entity/DTO/TutorDto";
import {Tutor} from "../Entity/User/Tutor";
import {CacheService} from "ng2-cache-service";
import {User, UserState} from "../Entity/User/User";
import {BasicResponse} from "../Entity/BasicResponse";



@Injectable()
export class TutorService {

  public static tokenHeader = 'Authorization';


  constructor(private http: HttpClient,private cacheService:CacheService) {

  }

  registerTutor(tutorDto: TutorDto) {
    return this.http.post(AppContextUrl + 'user/tutor' , tutorDto);
  }

  editTutor(id, tutor: Tutor) {
    return this.http.put(AppContextUrl + 'user/tutor/' + id, tutor);
  }

  getFavoriteTutorByStudent(){
    let user = this.cacheService.get('user') as User;
    let params = new HttpParams();

    return this.http.get<Array<Tutor>>(AppContextUrl + 'favoriteTutor/student/' + user.id);
  }

  getTutorByState(uState: UserState , page){
    let params = new HttpParams();
    params = params.append('uState', uState.toString() + '');
    params = params.append('page', page + '');
    return this.http.get<Array<Tutor>>(AppContextUrl + 'user/tutor', {params: params});
  }

  getTutorById(id){
    return this.http.get<Tutor>(AppContextUrl + 'user/tutor/' + id);
  }

  qualifyTutor(id, isQualify, reason) {
    return this.http.put<BasicResponse>(AppContextUrl + 'user/tutor/qualify/' + id + '?isQualify=' + isQualify + '&reason=' + reason, null);
  }





}
