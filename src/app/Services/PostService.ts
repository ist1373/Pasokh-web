import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {Province} from "../Entity/Province";
import {BasicResponse} from "../Entity/BasicResponse";
import {Category} from "../Entity/Category";
import {Post} from "../Entity/Post";
/**
 * Created by iman on 6/21/2018.
 */

@Injectable()
export class PostService {

  constructor(private http: HttpClient) { }


  getPostsByCategory(catId)
  {
    return this.http.get<Array<Post>>(AppContextUrl + 'post?catId=' + catId);
  }

  getPostById(postId)
  {
    return this.http.get<Post>(AppContextUrl + 'post/' + postId);
  }


  addPost(post)
  {
    return this.http.post<BasicResponse>(AppContextUrl + 'post', post);
  }
  updatePost(post)
  {
    return this.http.post<BasicResponse>(AppContextUrl + 'post/' + post.id, post);
  }
  deletePost(id)
  {
    return this.http.delete<BasicResponse>(AppContextUrl + 'post/' + id);
  }

}
