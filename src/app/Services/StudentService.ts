import { Injectable } from '@angular/core';
import {Headers, Http,RequestOptions} from "@angular/http";
import {Subject} from "rxjs/Subject";
import {JwtAuthenticationRequest} from "../Entity/Security/JwtAuthenticationRequest";
import {map} from "rxjs/operators";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {StudentDto} from "../Entity/DTO/StudentDto";
import {Student} from "../Entity/User/Student";
import {BasicResponse} from "../Entity/BasicResponse";
import {UserState} from "../Entity/User/User";



@Injectable()
export class StudentService {

  public static tokenHeader = 'Authorization';

  constructor(private http: HttpClient) {

  }

  registerStudent(studentDto: StudentDto) {
    return this.http.post(AppContextUrl + 'user/student' , studentDto);
  }

  editStudent(student: Student) {
    return this.http.put<BasicResponse>(AppContextUrl + 'user/student/' + student.id , student);
  }

  increaseStudentCredit(studentID: number, credit:number) {

    return this.http.post<BasicResponse>(AppContextUrl + 'user/student/' + studentID + '?credit=' + credit, null);
  }

  getStudentByState(uState: UserState, page){
    let params = new HttpParams();
    params = params.append('uState', uState.toString() + '');
    params = params.append('page', page + '');
    return this.http.get<Array<Student>>(AppContextUrl + 'user/student', {params: params});
  }

  getStudentById(id){

    return this.http.get<Student>(AppContextUrl + 'user/student/' + id);
  }


  // login(username: string, password: string) {
  //   // let params = new HttpParams();
  //   // params = params.append('captcha', captcha.toString());
  //
  //   var body = new JwtAuthenticationRequest();
  //   body.username = username;
  //   body.password = password;
  //
  //   return this.http.post(AppContextUrl + 'user/auth' , body);
  // }



  // getUser()
  // {
  //   return this.http.get(AppContextUrl + 'user',this.setAuthorizationHeader()).pipe(
  //     map(res => plainToClass<User, Object>(User, res.json()))
  //   )
  // }
  //
  // registerationConfirm(token:string)
  // {
  //   return this.http.get(AppContextUrl + 'user/registrationConfirm?token=' + token);
  // }
  //
  // getUsersByUserName(username)
  // {
  //   return this.http.get(AppContextUrl + 'user?username=' + username ,this.setAuthorizationHeader()).map(res =>{
  //     var bookMarket = plainToClass<User, Object[]>(User, res.json());
  //     return bookMarket;
  //   });
  // }
  //
  // // registerFinalUser(user:Student)
  // // {
  // //   return this.http.post(AppContextUrl + 'user/foreign-student',user);
  // // }
  //
  //
  // setAuthorizationHeader()
  // {
  //   let headers = new Headers({});
  //   headers.set('Authorization',this.cacheService.get('token'));
  //   let options = new RequestOptions({ headers: headers});
  //   return options;
  // }
  //
  // editUser(user:User)
  // {
  //   return this.http.put(AppContextUrl+'user/foreign-student',user,this.setAuthorizationHeader());
  // }


}
