import { Injectable } from '@angular/core';
import {Headers, Http,RequestOptions} from "@angular/http";
import {Subject} from "rxjs/Subject";
import {JwtAuthenticationRequest} from "../Entity/Security/JwtAuthenticationRequest";
import {map} from "rxjs/operators";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AppContextUrl} from "../app.globals";
import {StudentDto} from "../Entity/DTO/StudentDto";
import {Question, QuestionState} from "../Entity/Question";
import {CacheService} from "ng2-cache-service";
import {User} from "../Entity/User/User";
import {EndedQuestion, EndedQuestionState} from "../Entity/EndedQuestion";
import {BasicResponse} from "../Entity/BasicResponse";



@Injectable()
export class QuestionService {

  public static tokenHeader = 'Authorization';

  constructor(private http: HttpClient,private cacheService:CacheService) {

  }


  addQuestion(question: Question) {
    return this.http.post(AppContextUrl + 'question' , question);
  }


  getUserQuestions(page: number) {
    let user = this.cacheService.get('user') as User;
    let params = new HttpParams();

    params = params.append('page', page + '');

    return this.http.get<Array<Question>>(AppContextUrl + 'question/user/' + user.id , {params: params});
  }

  endQuestion(quesId, rate) {
    let user = this.cacheService.get('user') as User;
    return this.http.put<BasicResponse>(AppContextUrl + 'question/' + quesId + '/end?rate=' + rate, null);
  }

  getUserQuestionsByState(states: Array<QuestionState>, page: number){
    let user = this.cacheService.get('user') as User;
    let params = new HttpParams();

    params = params.append('page', page + '');
    params = params.append('size', 5 + '');

    for(let state of states)
    {
      params = params.append('qState', state.toString());
    }
    return this.http.get<Array<Question>>(AppContextUrl + 'question/user/' + user.id , {params: params});
  }

  getDirectQuestions(page: number) {
    let user = this.cacheService.get('user') as User;
    let params = new HttpParams();
    params = params.append('page', page + '');
    return this.http.get<Array<Question>>(AppContextUrl + 'question/tutor/' + user.id + '/direct', {params: params});
  }
  getPublicQuestions(page: number) {
    let user = this.cacheService.get('user') as User;
    let params = new HttpParams();
    params = params.append('page', page + '');
    return this.http.get<Array<Question>>(AppContextUrl + 'question/tutor/' + user.id + '/public', {params: params});
  }
  getActiveQuestionsByTutor(page: number) {
    let user = this.cacheService.get('user') as User;
    let params = new HttpParams();
    params = params.append('page', page + '');
    return this.http.get<Array<Question>>(AppContextUrl + 'question/tutor/' + user.id + '/active', {params: params});
  }



  getEndedQuestionByResponder(page: number) {
    let user = this.cacheService.get('user') as User;
    let params = new HttpParams();
    params = params.append('page', page + '');
    return this.http.get<Array<EndedQuestion>>(AppContextUrl + 'endedQuestion/tutor/' + user.id , {params: params});
  }

  // login(username: string, password: string) {
  //   // let params = new HttpParams();
  //   // params = params.append('captcha', captcha.toString());
  //
  //   var body = new JwtAuthenticationRequest();
  //   body.username = username;
  //   body.password = password;
  //
  //   return this.http.post(AppContextUrl + 'user/auth' , body);
  // }



  // getUser()
  // {
  //   return this.http.get(AppContextUrl + 'user',this.setAuthorizationHeader()).pipe(
  //     map(res => plainToClass<User, Object>(User, res.json()))
  //   )
  // }
  //
  // registerationConfirm(token:string)
  // {
  //   return this.http.get(AppContextUrl + 'user/registrationConfirm?token=' + token);
  // }
  //
  // getUsersByUserName(username)
  // {
  //   return this.http.get(AppContextUrl + 'user?username=' + username ,this.setAuthorizationHeader()).map(res =>{
  //     var bookMarket = plainToClass<User, Object[]>(User, res.json());
  //     return bookMarket;
  //   });
  // }
  //
  // // registerFinalUser(user:Student)
  // // {
  // //   return this.http.post(AppContextUrl + 'user/foreign-student',user);
  // // }
  //
  //
  // setAuthorizationHeader()
  // {
  //   let headers = new Headers({});
  //   headers.set('Authorization',this.cacheService.get('token'));
  //   let options = new RequestOptions({ headers: headers});
  //   return options;
  // }
  //
  // editUser(user:User)
  // {
  //   return this.http.put(AppContextUrl+'user/foreign-student',user,this.setAuthorizationHeader());
  // }


}
