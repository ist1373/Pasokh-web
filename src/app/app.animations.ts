import {trigger, state, animate, transition, style, keyframes} from "@angular/animations";
/**
 * Created by iman on 5/19/2018.
 */
export const shakeAnimation =
  trigger('shakeAnimation', [
    state('fix', style({transform: ' scale(1)'})),
    state('shake',   style({transform: ' scale(1)'})),
    transition('fix => shake', [
      animate(400,  keyframes([
        style({ transform: 'translate(1px, 1px) rotate(0deg)',     offset: 0}),
        style({ transform: 'translate(-1px, -2px) rotate(-1deg)',     offset: 0.1}),
        style({ transform: 'translate(-3px, 0px) rotate(1deg)',     offset: 0.2}),
        style({ transform: 'translate(3px, 2px) rotate(0deg)',     offset: 0.3}),
        style({ transform: 'translate(1px, -1px) rotate(1deg)',     offset: 0.4}),
        style({ transform: 'translate(-1px, 2px) rotate(-1deg)',     offset: 0.5}),
        style({ transform: 'translate(-3px, 1px) rotate(0deg)',     offset: 0.6}),
        style({ transform: 'translate(3px, 1px) rotate(-1deg)',     offset: 0.7}),
        style({ transform: 'translate(-1px, -1px) rotate(1deg)',     offset: 0.8}),
        style({ transform: 'translate(1px, 2px) rotate(0deg)',     offset: 0.9}),
      ]))
    ])
  ]);

export const slideInOutAnimation =
    trigger('flyInOut', [
      state('*', style({transform: 'translateX(0)'})),
      transition('void => *', [
        style({transform: 'translateX(-100%)'}),
        animate(200)
      ]),
      transition('* => void', [
        animate(200, style({transform: 'translateX(100%)'}))
      ])
    ]);



