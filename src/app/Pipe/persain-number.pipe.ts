import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'jalali-moment';

@Pipe({
  name: 'persainNumberPipe'
})
export class PersainNumberPipe implements PipeTransform {

  transform(value): string {
    try {
      let strValue = value.toString();
      strValue = strValue.replace(/1/g,'۱');
      strValue = strValue.replace(/2/g,'۲');
      strValue = strValue.replace(/3/g,'۳');
      strValue = strValue.replace(/4/g,'۴');
      strValue = strValue.replace(/5/g,'۵');
      strValue = strValue.replace(/6/g,'۶');
      strValue = strValue.replace(/7/g,'۷');
      strValue = strValue.replace(/8/g,'۸');
      strValue = strValue.replace(/9/g,'۹');
      strValue = strValue.replace(/0/g,'۰');
      return strValue;
    }
    catch(e){
      return value;
    }

  }

}
