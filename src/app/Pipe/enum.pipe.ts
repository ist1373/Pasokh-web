import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'jalali-moment';

@Pipe({
  name: 'enumPipe'
})
export class EnumPipe implements PipeTransform {
  transform(value): string {
    try {
      let strValue = value.toString();
     // Physics, Chemistry, Biology, Literature, Math, Arabic, Religious, English
      strValue = strValue.replace(/Archived/g,'تسویه شده');
      strValue = strValue.replace(/Completed/g,'تسویه نشده');

      strValue = strValue.replace(/Done/g,'تسویه شده');
      strValue = strValue.replace(/Cancel/g,'کنسل شده');
      strValue = strValue.replace(/Error/g,'خطا در عملیات');
      strValue = strValue.replace(/Pending/g,'در حال تسویه');


      strValue = strValue.replace(/Physics/g,'فیزیک');
      strValue = strValue.replace(/Chemistry/g,'شیمی');
      strValue = strValue.replace(/Biology/g,'زیست');
      strValue = strValue.replace(/Literature/g,'ادبیات');
      strValue = strValue.replace(/Math/g,'ریاضی');
      strValue = strValue.replace(/Arabic/g,'عربی');
      strValue = strValue.replace(/Religious/g,'دینی');
      strValue = strValue.replace(/English/g,'زبان');


      // Seventh, Eighth, Ninth, Tenth, Eleventh, Twelfth
      strValue = strValue.replace(/Seventh/g,'هفتم');
      strValue = strValue.replace(/Eighth/g,'هشتم');
      strValue = strValue.replace(/Ninth/g,'نهم');
      strValue = strValue.replace(/Tenth/g,'دهم');
      strValue = strValue.replace(/Eleventh/g,'یازدهم');
      strValue = strValue.replace(/Twelfth/g,'دوازدهم');


      strValue = strValue.replace(/ManyQuestion/g,'چندین سوال در تصویر است');
      strValue = strValue.replace(/NotQuestion/g,'تصویر نامربوط است');
      strValue = strValue.replace(/IrrevelentQuestion/g,'سوال با موضوع همخوانی ندارد');
      strValue = strValue.replace(/Other/g,'دیگر');


      strValue = strValue.replace(/NotAnswered/g,'پاسخ داده نشده است');
      strValue = strValue.replace(/WrongAnswered/g,'پاسخ اشتباه است');
      strValue = strValue.replace(/LateAnswered/g,'دیر پاسخ داده است');



      // strValue = strValue.replace(/Employee/g,'کارمند');
      // strValue = strValue.replace(/Grader/g,'محصل');
      // strValue = strValue.replace(/Student/g,'دانشجو');
      // strValue = strValue.replace(/SelfEmployed/g,'آزاد');
      // strValue = strValue.replace(/Housewife/g,'خانه دار');
      // strValue = strValue.replace(/NoJob/g,'بیکار');
      // strValue = strValue.replace(/Other/g,'سایر');
      //
      //
      // strValue = strValue.replace(/Father/g,'پدر');
      // strValue = strValue.replace(/Mother/g,'مادر');
      // strValue = strValue.replace(/Brother/g,'برادر');
      // strValue = strValue.replace(/Sister/g,'خواهر');
      // strValue = strValue.replace(/Children/g,'فرزند');
      //
      // strValue = strValue.replace(/Single/g,'مجرد');
      // strValue = strValue.replace(/Married/g,'متاهل');
      // strValue = strValue.replace(/Divorced/g,'جدا شده');
      //
      // strValue = strValue.replace(/Thesis/g,'تز تحصیلات عالی');
      // strValue = strValue.replace(/StudentExchange/g,'جابجایی دانشجو');
      // strValue = strValue.replace(/Personal/g,'شخصی');
      //
      // strValue = strValue.replace(/Bourse/g,'بورس');
      // strValue = strValue.replace(/Konkur/g,'کنکوری');
      // strValue = strValue.replace(/Open/g,'آزاد');
      // strValue = strValue.replace(/EducationalHistory/g,'سوابق تحصیلی');
      //
      // strValue = strValue.replace(/First/g,'اول');
      // strValue = strValue.replace(/Second/g,'دوم');
      //
      // strValue = strValue.replace(/Irreligious/g,'بدون دین');
      // strValue = strValue.replace(/Islam/g,'اسلام');
      // strValue = strValue.replace(/Jewish/g,'یهودی');
      // strValue = strValue.replace(/Christianity/g,'مسیحی');
      // strValue = strValue.replace(/Hindu/g,'هندو');
      // strValue = strValue.replace(/Other/g,'ادیان دیگر');
      //



      return strValue;
    }
    catch(e){
      return value;
    }
  }
}


