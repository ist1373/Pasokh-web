import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'jalali-moment';

@Pipe({
  name: 'jalaliPipe'
})
export class JalaliPipePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let MomentDate = moment(value);
    return MomentDate.format("jYYYY/jM/jD");
  }
}
