/**
 * Created by iman on 5/21/2018.
 */


export let port = '8080'
export let host = 'localhost'
export let AppContextUrl = 'http://' + host + ':' + port + '/api/v1/';
export let FileContextUrl = 'http://' + host + ':' + port + '/api/v1/';


// export let port = '443'
// export let host = 'pasokh20.com'
// export let AppContextUrl = 'https://' + host + ':' + port + '/api/v1/';
// export let FileContextUrl = 'https://' + host + ':' + port + '/api/v1/';


export let AngularPort = '4200'
export let AngularHost = 'localhost'
export let AngularClientUrl = 'http://' + AngularHost + ':' + AngularPort + '/'


export let CreditScale = 'ایپاسخ'
export let CreditValue = 500

export function serializeType<T>(object: T) {
  return function () { return object}
}



export let GradeSelect = [
  {value: 'Seventh', label: 'مقطع هفتم'},
  {value: 'Eighth', label: 'مقطع هشتم'},
  {value: 'Ninth', label: 'مقطع نهم'},
  {value: 'Tenth', label: 'مقطع دهم'},
  {value: 'Eleventh', label: 'مقطع یازدهم'},
  {value: 'Twelfth', label: 'مقطع دوازدهم'},
]


export let FieldSelect = [
  {value: 'Math', label: 'ریاضی فیزیک'},
  {value: 'NaturalScience', label: 'علوم تجربی'},
  {value: 'Humanities', label: 'علوم انسانی'},
  {value: 'Technical', label: 'فنی حرفه ای'},
]


export let ReportTypeSelect = [
  {value: 'ManyQuestion', label: 'چندین سوال در تصویر است'},
  {value: 'NotQuestion', label: 'تصویر نامربوط است'},
  {value: 'IrrevelentQuestion', label: 'سوال با موضوع همخوانی ندارد'},
  {value: 'Other', label: 'دیگر'},
]

export let ReportTopicSelect = [
{value: 'NotAnswered', label: 'پاسخ داده نشده است'},
{value: 'WrongAnswered', label: 'پاسخ اشتباه است'},
{value: 'LateAnswered', label: 'دیر پاسخ داده است'},
{value: 'Other', label: 'دیگر'},
]
export let RolesSelect = [
{value: 'student', label: 'دانش آموز'},
{value: 'tutor', label: 'پاسخ گو'},
{value: 'employee', label: 'کارمند'},
{value: 'admin', label: 'مدیر'},
]

export let UserStateSelect = [
  {value: 'Pending', label: 'در حال بررسی'},
  {value: 'Verified', label: 'تایید شده'},
  {value: 'Qualified', label: 'صحت سنجی شده'},
  {value: 'Rejected', label: 'رد شده'},
  {value: 'Banned', label: 'محروم شده'},
  {value: 'NeedToEdit', label: 'نیاز به تغییر'},
  {value: 'Deleted', label: 'حذف شده'},
]
