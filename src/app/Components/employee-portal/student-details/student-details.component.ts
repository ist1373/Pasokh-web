import {Component, OnInit, ViewChild} from '@angular/core';
import {LocationService} from "../../../Services/LocationService";
import {GradeSelComponent} from "../../sign-in-up/grade-sel/grade-sel.component";
import {AppContextUrl, FieldSelect, FileContextUrl, GradeSelect} from "../../../app.globals";
import {UserService} from "../../../Services/UserService";
import {Field, Student} from "../../../Entity/User/Student";
import {FormControl, FormGroup} from "@angular/forms";
import {City} from "../../../Entity/City";
import {Grade} from "../../../Entity/Question";
import {AccessType, UploadedFile} from "../../../Entity/UploadedFile";
import {FileService} from "../../../Services/FileService";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";
import {StudentService} from "../../../Services/StudentService";
import {BasicResponse} from "../../../Entity/BasicResponse";
import {SelectComponent} from "ng-select";
import {ActivatedRoute} from "@angular/router";


@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.scss'],
  providers:[LocationService, UserService, FileService, StudentService]
})
export class StudentDetailsComponent implements OnInit {


  student: Student;
  passChange = false;

  provinceSelect: Array<any> = [];
  citySelect: Array<any> = [];
  gradeSelect: Array<any> = [];
  fieldSelect: Array<any> = [];

  avatar: UploadedFile;
  city: City;
  grade: Grade;
  field: Field;
  baseFile = FileContextUrl;

  @ViewChild('province') provinceNgSelect: SelectComponent;
  @ViewChild('city') cityNgSelect: SelectComponent;
  @ViewChild('grade') gradeNgSelect: SelectComponent;
  @ViewChild('field') fieldNgSelect: SelectComponent;

  // studentForm: FormGroup;
  // firstName = new FormControl('');
  // lastName = new FormControl('');
  // school = new FormControl('');
  // topic = new FormControl('');
  // grade = new FormControl('');
  // avg = new FormControl('');
  // email = new FormControl('');
  // password = new FormControl('');





  constructor(private activeRoute: ActivatedRoute,private locationService: LocationService,private studentService: StudentService,private userService: UserService,private fileService: FileService,private snackBar: MatSnackBar) {
    // this.studentForm = new FormGroup({
    //   firstName: this.firstName,
    //   lastName:this.lastName,
    //   school:this.school,
    //   topic:this.topic,
    //   grade:this.grade,
    //   avg:this.avg,
    //   email:this.email,
    //   password:this.password,
    // });

    this.activeRoute.params.subscribe(res => {
      this.loadUser(res['id']);
    })

  }

  ngOnInit() {
    this.initProvinces()
    this.initGrades();
    this.initField();

  }

  loadUser(id){
    this.studentService.getStudentById(id).subscribe(res => {
      console.log(res)
      this.student = res as Student;
      if(this.student.profileImage != null)
      {
        this.avatar = this.student.profileImage
      }
      setTimeout(() => {
        if (this.student.grade != null)
        {
          this.gradeNgSelect.select(this.student.grade.toString());
        }
        if (this.student.field != null)
        {
          this.fieldNgSelect.select(this.student.field.toString());
        }
        if (this.student.city != null)
        {
          this.provinceNgSelect.select(this.student.city.province.id.toString());
          this.initCity(this.student.city.province.id);
        }
      }, 200)

    })
  }
  onChangeCredit(credit:number , creditmodal){
    console.log(credit)
    this.studentService.increaseStudentCredit(this.student.id, credit - this.student.credit).subscribe(res => {
      if ((res as BasicResponse).success)
      {
        this.openSnackBar('تغییرات با موفقیت ذخیره شد','success')
        creditmodal.hide();
      }
    })
  }

  initProvinces(){
    this.locationService.getProvinces().subscribe(provs =>{
      for(let prov of provs)
      {
        console.log(prov)
        this.provinceSelect.push({value:prov.id + '',label:prov.title.toString()})
      }
    })
  }

  initCity(id){
    this.citySelect = [];
    this.locationService.getCities(id).subscribe(cities =>{
      for(let c of cities)
      {
        console.log(c)
        this.citySelect.push({value: c.id + '', label: c.title.toString()})
      }
      this.setCity();
      // if(this.student.city != null)
      // {
      //   console.log(this.student.city.id.toString())
      //   this.cityNgSelect.select(this.student.city.id.toString());
      // }
    })
  }


  setCity()
  {
    setTimeout(() => {
      if(this.student.city != null)
      {
        this.cityNgSelect.select(this.student.city.id.toString());
      }
    }, 200);
  }


  initGrades()
  {
    this.gradeSelect = GradeSelect;
  }

  initField()
  {
    this.fieldSelect = FieldSelect;
  }
  showPass(pass)
  {
    if(pass != 'password')
    {
      this.passChange = true;
    }
  }


  onSelectProvince(item) {
    console.log(item.value);
    this.initCity(item.value);
  }

  onSelectCity(item) {
    console.log(item.value);
    this.city = new City()
    this.city.id = item.value;
  }
  onSelectGrade(item)
  {
    this.grade = item.value;
  }

  onSelectField(item)
  {
    this.field = item.value;
  }

  onPickProfile(event){
    this.fileService.uploadFile(event.target.files[0], AccessType[AccessType.Public] , '').subscribe((response) => {
      if (response['success'] == true) {
        this.avatar = new UploadedFile();
        this.avatar.id = response['message'];
      }
    })
  }

  onEdit(fname, lname, email, pass, reppass, school , avg , credit , creditmodal)
  {

    if(credit != this.student.credit)
    {
      creditmodal.show();
    }

    if(pass == reppass && pass == 'password')
    {
      console.log(fname,lname,email, pass, reppass, school, avg)
      // this.student.password = pass
      this.student.firstName = fname
      this.student.lastName = lname
      this.student.email = email
      this.student.school = school
      this.student.average = avg
      this.student.city = this.city;      // =! null ? this.city : null
      this.student.field = this.field;
      this.student.grade = this.grade;
      this.student.profileImage = this.avatar;

      // this.student.mobileNumber =
      console.log(this.student)
      this.saveStudent()
    }
    else if (pass == reppass && pass != 'password')
    {
      this.student.password = pass
      this.student.firstName = fname
      this.student.lastName = lname
      this.student.email = email
      this.student.school = school
      this.student.average = avg
      this.student.city = this.city;      // =! null ? this.city : null
      this.student.field = this.field;
      this.student.grade = this.grade;
      this.student.profileImage = this.avatar;
      this.saveStudent();
    }
    else
    {
      this.openSnackBar('رمزعبور و تکرار آن یکسان نیستند' , 'error');
    }
  }
  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }

  saveStudent()
  {
    this.studentService.editStudent(this.student).subscribe(res => {
      let bres = res as BasicResponse;
      if (bres.success)
      {
        this.openSnackBar('تغییرات با موفقیت ذخیره شد','success')
      }
      else
      {
        this.openSnackBar(bres.message,'error')
      }
    })
  }

}
