import {Component, OnInit, ViewChild} from '@angular/core';
import {TutorService} from "../../../Services/TutorService";
import {User, UserState} from "../../../Entity/User/User";
import {StudentService} from "../../../Services/StudentService";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {CacheService} from "ng2-cache-service";
import {RolesSelect, UserStateSelect} from "../../../app.globals";
import {AdminService} from "../../../Services/AdminService";
import {EmployeeService} from "../../../Services/EmployeeService";
import {SelectComponent} from "ng-select";
import {ScrollEvent} from "ngx-scroll-event";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  providers:[TutorService, StudentService , EmployeeService , AdminService,CacheService]
})
export class UserListComponent implements OnInit {

  page = 0;
  userTypeShow = 'student'
  uState = 'Pending'
  userRole = ''
  // verificationState = '' // = 'verified'
  // qualificationState = ''  // = 'qualified'
  users: Array<User> = [];
  rolesSelect: Array<any> = []
  userStateSelect: Array<any> = []
  spinner = false
  isLoadingData = false

  @ViewChild('userTypeSelect') userTypeSelect: SelectComponent;
  @ViewChild('userState') userState: SelectComponent;

  constructor(private cacheService: CacheService, private activatedRoute: ActivatedRoute, private router: Router ,private tutorService: TutorService , private studentService: StudentService , private employeeService: EmployeeService , private adminService: AdminService) { }

  ngOnInit() {
    this.rolesSelect = RolesSelect;
    this.userStateSelect = UserStateSelect;
    this.activatedRoute.queryParams.subscribe(params => {
      if(params['userType'] != null)
      {
        this.userTypeShow = params['userType'];
      }
      else
      {
        this.userTypeShow = 'student';
        const queryParams: Params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
        queryParams['userType'] = this.userTypeShow;
        this.router.navigate([], { queryParams: queryParams });
      }

      if(params['userState'] != null)
      {
        this.uState = params['userState'];
      }
      else
      {
        this.uState = 'Pending';
        const queryParams: Params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
        queryParams['userState'] = this.uState;
        this.router.navigate([], { queryParams: queryParams });
      }

      this.loadUsers();
    })



    if(this.cacheService.get('userType') != null)
    {
      this.userRole = this.cacheService.get('userType');
      if(this.userRole == 'ROLE_EMPLOYEE')
      {
        this.rolesSelect.splice(2, 2);
      }
    }




  }

  onChangeUserType(event) {
    this.userTypeShow = event.value;
    this.page = 0;
    this.users = []

    const queryParams: Params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
    queryParams['userType'] = event.value;
    this.router.navigate([], { queryParams: queryParams });
    this.loadUsers()
  }
  onChangeUserState(event) {
    this.uState = event.value;
    console.log('immmmmmmmmmmm');
    this.page = 0;
    this.users = []

    const queryParams: Params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
    queryParams['userState'] = event.value;
    this.router.navigate([], { queryParams: queryParams });
    this.loadUsers();
  }



  loadUsers()
  {
    this.spinner = true
    this.isLoadingData = true

    setTimeout(() => {
      this.userTypeSelect.select(this.userTypeShow)
      this.userState.select(this.uState)
    } , 100);


    if(this.userTypeShow == 'student')
    {
      this.studentService.getStudentByState(UserState[this.uState], this.page).subscribe(users => {

        if(this.page == 0){
          this.users = users;
        }
        else {
          for(let u of users)
          {

            this.users.push(u);
          }
        }

        if(users.length > 0) {
          this.isLoadingData = false
        }
        this.spinner = false
        console.log(users)
      });
    }
    else if(this.userTypeShow == 'tutor')
    {

      this.tutorService.getTutorByState(UserState[this.uState], this.page).subscribe(users => {
        if(this.page == 0){
          this.users = users;
        }
        else {
          for(let u of users)
          {
            this.users.push(u);
          }
        }
        if(users.length > 0) {
          this.isLoadingData = false
        }
        this.spinner = false
        console.log(users)
      });
    }
    else if (this.userTypeShow == 'employee')
    {
      this.employeeService.getAllEmployees(this.page).subscribe(users => {
        if(this.page == 0){
          this.users = users;
        }
        else {
          for(let u of users)
          {
            this.users.push(u);
          }
        }
        if(users.length > 0) {
          this.isLoadingData = false
        }
        this.spinner = false
        console.log(users);
      });
    }
    else if (this.userTypeShow == 'admin')
    {
      this.adminService.getAllAdmins(this.page).subscribe(users => {
        if(this.page == 0){
          this.users = users;
        }
        else {
          for(let u of users)
          {
            this.users.push(u);
          }
        }
        if(users.length > 0) {
          this.isLoadingData = false
        }
        this.spinner = false
        console.log(users);
      });
    }
  }

  onSelectUser(user)
  {

    if (this.userTypeShow == 'student')
    {
      this.router.navigate(['/employee/studentDetails/' , user.id]);
    }
    else if(this.userTypeShow == 'tutor')
    {
      this.router.navigate(['/employee/tutorDetails/' , user.id]);
    }
    else if(this.userTypeShow == 'employee')
    {
      this.router.navigate(['/employee/employeeDetails/' , user.id]);
    }
    else if(this.userTypeShow == 'admin')
    {
      this.router.navigate(['/employee/employeeDetails/' , user.id]);
    }
  }
  onSearch(email,phone)
  {

  }


  public handleScroll(event: ScrollEvent) {
    if (event.isReachingBottom) {
      if (!this.isLoadingData)
      {
        this.page += 1
        this.loadUsers()
        console.log(`the user is reaching the bottom`);
      }
    }
  }
}
