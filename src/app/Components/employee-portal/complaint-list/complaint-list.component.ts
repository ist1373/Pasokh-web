import { Component, OnInit } from '@angular/core';
import {ComplaintService} from "../../../Services/ComplaintService";
import {Question, ReportTopic} from "../../../Entity/Question";
import {FileContextUrl, ReportTopicSelect, ReportTypeSelect} from "../../../app.globals";
import {ComplaintReport} from "../../../Entity/ComplaintReport";
import {ActivatedRoute, NavigationExtras, Params, Router} from "@angular/router";
import {CacheService} from "ng2-cache-service";
import {QuestionReportService} from "../../../Services/QuestionReportService";
import {QuestionReport} from "../../../Entity/QuestionReport";
import {ScrollEvent} from "ngx-scroll-event";

@Component({
  selector: 'app-complaint-list',
  templateUrl: './complaint-list.component.html',
  styleUrls: ['./complaint-list.component.scss'],
  providers: [ComplaintService,QuestionReportService,CacheService]
})
export class ComplaintListComponent implements OnInit {

  showQuestionReports = true;
  page = 0;
  topicsSelect;
  questionReportSelect;
  topic = 'NotAnswered'
  quesRepType = 'Other'
  spinner = false
  isLoadingData = false
  complaintReports : Array<ComplaintReport> = []
  questionReports : Array<QuestionReport> = []

  baseFile = FileContextUrl

  selectedQuestion:Question;

  constructor(private activatedRoute: ActivatedRoute, private router:Router,private cacheService: CacheService, private complaintService: ComplaintService, private  questionReportService: QuestionReportService) { }

  ngOnInit() {
    this.topicsSelect = ReportTopicSelect;
    this.questionReportSelect = ReportTypeSelect;

    this.activatedRoute.queryParams.subscribe(params => {
      if(params['type'] == 'tutor')
      {
        if(params['reportType'] != null)
        {
          this.topic = params['reportType'];
        }
        else
        {
          this.topic = 'NotAnswered';
          const queryParams: Params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
          queryParams['reportType'] = this.topic;
          this.router.navigate([], { queryParams: queryParams });
        }
        this.onSelectComplaintReports();
      }
      else
      {
        if(params['reportType'] != null)
        {
          this.quesRepType = params['reportType'];
        }
        else
        {
          this.quesRepType = 'ManyQuestion';
          const queryParams: Params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
          queryParams['reportType'] = this.quesRepType;
          this.router.navigate([], { queryParams: queryParams });
        }
        this.onSelectQuestionReports();

      }
      //
    })


  }

  onSelectQuestionReports()
  {
    this.page = 0
    this.complaintReports = []
    this.questionReports = []
    this.showQuestionReports = true;
    this.loadComplaints();
  }


  onSelectComplaintReports()
  {
    this.page = 0
    this.complaintReports = []
    this.questionReports = []
    this.showQuestionReports = false;
    this.loadComplaints();
  }

  loadComplaints()
  {
    this.isLoadingData = true
    if(this.showQuestionReports) {
      const queryParams: Params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
      queryParams['type'] = 'question';
      this.router.navigate([], { queryParams: queryParams });

      this.spinner = true
      this.questionReportService.getQuestionReports(this.quesRepType , this.page).subscribe(res => {
        console.log(res)
        this.spinner = false;
        if(this.page == 0){
          this.questionReports = res;
        }
        else {
          for(let cr of res)
          {
            this.questionReports.push(cr);
          }
        }
        if(res.length > 0)
        {
          this.isLoadingData = false
        }
      })
    }
    else {
      const queryParams: Params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
      queryParams['type'] = 'tutor';
      this.router.navigate([], { queryParams: queryParams });


      this.spinner = true
      this.complaintService.getReportByTopic(this.topic , this.page).subscribe(res => {
        console.log(res)
        this.spinner = false;
        if(this.page == 0){
          this.complaintReports = res;
        }
        else {
          for(let cr of res)
          {
            this.complaintReports.push(cr);

          }
        }
        if(res.length > 0)
        {
          this.isLoadingData = false
        }

      })
    }

  }




  onChangeComplaintType(event){
    this.topic = event.value;

    const queryParams: Params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
    queryParams['reportType'] = this.topic;
    this.router.navigate([], { queryParams: queryParams });

    this.onSelectComplaintReports();
  }

  onChangeQuestionReportType(event){
    this.quesRepType = event.value;

    const queryParams: Params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
    queryParams['reportType'] = this.quesRepType;
    console.log(queryParams['reportType'])

    this.router.navigate([], { queryParams: queryParams });

    this.onSelectQuestionReports();
  }



  onShowChatroom(q)
  {
    this.cacheService.set('current-q', q);
    let navigationExtras: NavigationExtras = {
      queryParams: { 'grade': q.grade, 'topic': q.questionTopic, 'qId': q.id},
    };
    this.router.navigate(['/employee/chatRoom/' + q.chatRoom.uid], navigationExtras);
  }

  onShowResponder(q){
    this.router.navigate(['/employee/tutorDetails/' + q.responder.id]);
  }
  onShowQuestioner(q){
    this.router.navigate(['/employee/studentDetails/' + q.questioner.id]);
  }
  onShowQuestion(q, questionModal){
    this.selectedQuestion = q;
    questionModal.show();
  }

  public handleScroll(event: ScrollEvent) {
    if (event.isReachingBottom) {
      if (!this.isLoadingData)
      {
        this.page += 1
        this.loadComplaints()
        console.log(`the user is reaching the bottom`);
      }
    }
  }
}
