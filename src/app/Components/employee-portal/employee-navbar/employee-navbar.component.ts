import {Component, Input, OnInit} from '@angular/core';
import {CacheService} from "ng2-cache-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-employee-navbar',
  templateUrl: './employee-navbar.component.html',
  styleUrls: ['./employee-navbar.component.scss'],
  providers:[CacheService]
})
export class EmployeeNavbarComponent implements OnInit {

  @Input()
  fontColor = ''

  @Input()
  background = '#fafafa!important'

  @Input()
  title = 'پنل کارمندان'


  userRole = ''

  constructor(private cacheService: CacheService, private router: Router) { }

  ngOnInit() {
    if(this.cacheService.get('userType') != null)
    {
      this.userRole = this.cacheService.get('userType');
      if (this.userRole == 'ROLE_ADMIN'){
        this.title = 'پنل مدیریت';
      }

    }
  }

  logout()
  {
    this.cacheService.removeAll();
    this.router.navigate(['/signIn']);
  }

}
