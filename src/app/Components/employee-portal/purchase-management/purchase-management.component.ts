import { Component, OnInit } from '@angular/core';
import {PurchaseService} from "../../../Services/PurchaseService";
import {Purchase} from "../../../Entity/Purchase";
import {AppContextUrl, CreditScale, FileContextUrl} from "../../../app.globals";
import {TransactionService} from "../../../Services/TransactionService";
import {BasicResponse} from "../../../Entity/BasicResponse";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";
import {User} from "../../../Entity/User/User";
import {UserService} from "../../../Services/UserService";
import {Student} from "../../../Entity/User/Student";
import {StudentService} from "../../../Services/StudentService";
import {AccessType, UploadedFile} from "../../../Entity/UploadedFile";
import {FileService} from "../../../Services/FileService";


@Component({
  selector: 'app-purchase-management',
  templateUrl: './purchase-management.component.html',
  styleUrls: ['./purchase-management.component.scss'],
  providers: [PurchaseService, TransactionService, UserService , StudentService , FileService]
})
export class PurchaseManagementComponent implements OnInit {

  baseFile = FileContextUrl
  availablePurchase: Array<Purchase>;
  creditScale = CreditScale
  spinner = false
  selectedPurchase: Purchase;
  isNewPurchase = false;

  constructor(private fileService: FileService, private studentService: StudentService, private userService: UserService, private router: Router ,private purchaseService: PurchaseService, private transactionService: TransactionService,private ativatedRoute: ActivatedRoute,private snackBar:MatSnackBar) { }

  ngOnInit() {
    this.loadPurchase();

  }

  // loadUser()
  // {
  //   this.userService.getMyProfile().subscribe(user => {
  //
  //   })
  // }

  loadPurchase()
  {
    this.spinner = true
    this.purchaseService.getAllPurchase().subscribe(perchases => {
      console.log(perchases)
      this.availablePurchase = perchases as Array<Purchase>;
      this.spinner = false;
    })
  }

  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }

  onEditPurchase(purchase: Purchase, purchaseModal)
  {
    this.isNewPurchase = false
    this.selectedPurchase = purchase;
    purchaseModal.show();
  }
  onDeletePurchase(purchase: Purchase)
  {

  }
  onNewPurchase(purchaseModal){
    this.isNewPurchase = true
    this.selectedPurchase = new Purchase();
    this.selectedPurchase.title = ''
    this.selectedPurchase.price = 0
    this.selectedPurchase.credit = 0
    purchaseModal.show();
  }

  onSavePurchase(purchaseModal, title, price, credit)
  {
    this.selectedPurchase.title = title
    this.selectedPurchase.credit = credit
    this.selectedPurchase.price = price
     if(!this.isNewPurchase)
     {
        this.purchaseService.editPurchase(this.selectedPurchase).subscribe(res => {
          if ((res as BasicResponse).success){
            purchaseModal.hide();
            this.openSnackBar('تغییرات با موفقیت انجام شد' , 'success')
          }
        })
     }
     else{
       this.purchaseService.addPurchase(this.selectedPurchase).subscribe(res => {
         if ((res as BasicResponse).success){
           purchaseModal.hide();
           this.openSnackBar('تغییرات با موفقیت انجام شد' , 'success')
           this.availablePurchase.push(this.selectedPurchase);
         }
       })
     }
  }


  onPickCatImg(event) {
    this.spinner = true
    this.fileService.uploadFile(event.target.files[0], AccessType[AccessType.Public] , '').subscribe((response) => {
      if (response['success'] == true) {
        this.spinner = false
        let uf = new UploadedFile();
        uf.id = response['message'];
        this.selectedPurchase.image = uf;
      }
    })
  }

  onChangeShowState(event) {
    if(event.value == 'show')
    {
      this.selectedPurchase.isShow = true;
    }
    else {
      this.selectedPurchase.isShow = false;
    }


  }

}

