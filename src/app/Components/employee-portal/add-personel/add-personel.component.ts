import {Component, OnInit, ViewChild} from '@angular/core';
import {LocationService} from "../../../Services/LocationService";
import {GradeSelComponent} from "../../sign-in-up/grade-sel/grade-sel.component";
import {AppContextUrl, FieldSelect, FileContextUrl, GradeSelect} from "../../../app.globals";
import {UserService} from "../../../Services/UserService";
import {Field, Student} from "../../../Entity/User/Student";
import {FormControl, FormGroup} from "@angular/forms";
import {City} from "../../../Entity/City";
import {Grade} from "../../../Entity/Question";
import {AccessType, UploadedFile} from "../../../Entity/UploadedFile";
import {FileService} from "../../../Services/FileService";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";
import {StudentService} from "../../../Services/StudentService";
import {BasicResponse} from "../../../Entity/BasicResponse";
import {SelectComponent} from "ng-select";
import {ActivatedRoute} from "@angular/router";
import {EmployeeService} from "../../../Services/EmployeeService";
import {User, UserState} from "../../../Entity/User/User";
import {Employee} from "../../../Entity/User/Employee";
import {Admin} from "../../../Entity/User/Admin";
import {AdminService} from "../../../Services/AdminService";


@Component({
  selector: 'app-add-personel',
  templateUrl: './add-personel.component.html',
  styleUrls: ['./add-personel.component.scss'],
  providers:[LocationService, UserService, FileService, EmployeeService , AdminService]
})
export class AddPersonelComponent implements OnInit {


  user: User;
  passChange = false;
  personnelType = 'employee';

  avatar: UploadedFile;

  baseFile = FileContextUrl;







  constructor(private adminService: AdminService , private activeRoute: ActivatedRoute,private locationService: LocationService,private employeeService: EmployeeService,private userService: UserService,private fileService: FileService,private snackBar: MatSnackBar) {

  }

  ngOnInit() {

  }


  onChangeState(event) {
    // this.qualificationState = event.value;
  }
  onChangePersonnelType(event) {
    this.personnelType = event.value;
    console.log(event.value)
  }

  showPass(pass)
  {
    if(pass != 'password')
    {
      this.passChange = true;
    }
  }
  onPickProfile(event){
    this.fileService.uploadFile(event.target.files[0], AccessType[AccessType.Public] , '').subscribe((response) => {
      if (response['success'] == true) {
        this.avatar = new UploadedFile();
        this.avatar.id = response['message'];
      }
    })
  }
  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }

  saveEmployee(fname, lname, email , phone , pass , passrep)
  {
    if(fname == null || fname.length == 0)
    {
      this.openSnackBar('لازم است نام کارمند وارد شود', 'error');
      return null;
    }
    else if(lname == null || lname.length == 0)
    {
      this.openSnackBar('لازم است نام خانوادگی کارمند وارد شود', 'error');
      return null;
    }
    else if(email == null || email.length == 0)
    {
      this.openSnackBar('لازم است نام ایمیل کارمند وارد شود', 'error');
      return null;
    }
    else if(phone == null || phone.length == 0)
    {
      this.openSnackBar('لازم است شماره همراه کارمند وارد شود', 'error');
      return null;
    }
    else if(pass == null || pass.length == 0)
    {
      this.openSnackBar('لازم است رمز عبور کارمند وارد شود', 'error');
      return null;
    }
    else if(pass.length < 6)
    {
      this.openSnackBar('حداقل طول رمز عبور ۶ کاراکتر می باشد', 'error');
      return null;
    }
    else if(pass != passrep)
    {
      this.openSnackBar('رمز عبور با تکرار آن برابر نیست.', 'error');
      return null;
    }

    if( this.personnelType == 'employee')
    {
      let emp = new Employee()
      emp.firstName = fname
      emp.lastName = lname
      emp.mobileNumber = phone
      emp.email = email
      emp.password = pass;
      emp.userState = UserState.Pending  //TODO set appropriate state
      //emp.qualified = false



      this.employeeService.registerEmployee(emp).subscribe(res => {
        let r = res as BasicResponse
        if (r.success)
        {
          this.openSnackBar('کارمند با موفقیت ثبت نام شد.', 'success');
        }
        else
        {
          this.openSnackBar(r.message, 'error');
        }
      }, error => {
        this.openSnackBar((error.error as BasicResponse).message, 'error');
      })
    }
    else
    {
      let adm = new Admin()
      adm.firstName = fname
      adm.lastName = lname
      adm.mobileNumber = phone
      adm.email = email
      adm.password = pass;
      adm.userState = UserState.Pending  //TODO set appropriate state



      this.adminService.registerAdmin(adm).subscribe(res => {
        let r = res as BasicResponse
        if (r.success)
        {
          this.openSnackBar('مدیر با موفقیت ثبت نام شد.', 'success');
        }
        else
        {
          this.openSnackBar(r.message, 'error');
        }
      }, error => {
        this.openSnackBar((error.error as BasicResponse).message, 'error');
      });
    }
    // this.employeeService.(this.student).subscribe(res => {
    //   let bres = res as BasicResponse;
    //   if (bres.success)
    //   {
    //     this.openSnackBar('تغییرات با موفقیت ذخیره شد','success')
    //   }
    //   else
    //   {
    //     this.openSnackBar(bres.message,'error')
    //   }
    // })
  }

}
