import { Component, OnInit } from '@angular/core';
import {FileContextUrl} from "../../../app.globals";
import {AccessType, UploadedFile} from "../../../Entity/UploadedFile";
import {UserService} from "../../../Services/UserService";
import {Tutor} from "../../../Entity/User/Tutor";
import {FileService} from "../../../Services/FileService";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";
import {TutorService} from "../../../Services/TutorService";
import {BasicResponse} from "../../../Entity/BasicResponse";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-tutor-details',
  templateUrl: './tutor-details.component.html',
  styleUrls: ['./tutor-details.component.scss'],
  providers: [UserService, FileService, TutorService]
})
export class TutorDetailsComponent implements OnInit {

  tutor: Tutor;
  avatar: UploadedFile;
  baseUrl = FileContextUrl

  spinner = false

  constructor(private activeRoute:ActivatedRoute,private tutorService: TutorService, private userService: UserService,private fileService: FileService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.activeRoute.params.subscribe(res => {
      this.loadUser(res['id']);

    });

  }

  loadUser(id){
    this.tutorService.getTutorById(id).subscribe(res => {
      console.log(res)
      this.tutor = res as Tutor;
      if (this.tutor.profileImage != null)
      {
        this.avatar = this.tutor.profileImage
      }
      setTimeout(() => {

      }, 200)
    })
  }

  onChangeTutorGender(event) {
    this.tutor.gender = event.value
  }
  onChangeTutorTopic(event)
  {
    console.log(event.value)
    this.tutor.topics = event.value
  }

  onPickProfile(event){
    this.fileService.uploadFile(event.target.files[0], AccessType[AccessType.Public] , '').subscribe((response) => {
      if (response['success'] == true) {
        this.avatar = new UploadedFile();
        this.avatar.id = response['message'];
      }
    })
  }
  onEdit(fname, lname, email, pass, passrep, phone, biography)
  {
    if( pass == passrep && pass == 'password')
    {
      if (fname != null)
        this.tutor.firstName = fname;
      if (lname != null)
        this.tutor.lastName = lname;
      if (email != null)
        this.tutor.email = email
      if (phone != null)
        this.tutor.mobileNumber = phone
      if (biography != null)
        this.tutor.biography = biography
      if (this.avatar != null)
        this.tutor.profileImage = this.avatar;

      this.saveTutor()
    }
    else if (pass != passrep )
    {
      this.openSnackBar('رمزعبور و تکرار آن یکسان نیستند' , 'error');
    }
    else if (pass == passrep && pass.length <6 )
    {
      this.openSnackBar('حداقل طول رمز عبور ۶ کاراکتر می باشد.' , 'error');
    }
    else
    {
      this.tutor.password = pass
      if (fname != null)
        this.tutor.firstName = fname;
      if (lname != null)
        this.tutor.lastName = lname;
      if (email != null)
        this.tutor.email = email
      if (phone != null)
        this.tutor.mobileNumber = phone
      if (biography != null)
        this.tutor.biography = biography
      if (this.avatar != null)
        this.tutor.profileImage = this.avatar;
      this.saveTutor()
    }
    console.log(this.tutor)
  }

  saveTutor()
  {
    this.spinner = true
    this.tutorService.editTutor(this.tutor.id,this.tutor).subscribe(res => {
      if ((res as BasicResponse).success)
      {
        this.openSnackBar('تغییرات با موفقیت ذخیره شد.' , 'success');
      }
      else
      {
        this.openSnackBar((res as BasicResponse).message , 'error');
      }
      this.spinner = false;
    })
  }

  qualifyTutor(isQualify,reason){
    this.tutorService.qualifyTutor(this.tutor.id, isQualify, reason).subscribe(res =>{
      let r = res as BasicResponse;
      if (r.success) {
        this.openSnackBar( 'عملیات با موفقیت انجام شد.' , 'success');
      }
      else{
        this.openSnackBar( r.message , 'error');
      }
    })
  }


  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }


}
