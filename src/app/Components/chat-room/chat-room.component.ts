import {Component, HostListener, OnInit} from '@angular/core';
import {ChatMessage, MessageType} from "../../Entity/ChatMessage";
import {ChatMessageService} from "../../Services/ChatMessageService";
import {ActivatedRoute} from "@angular/router";
import {CacheService} from "ng2-cache-service";
import {User} from "../../Entity/User/User";
import {AppContextUrl, ReportTopicSelect} from "../../app.globals";
import {ChatRoom} from "../../Entity/ChatRoom";
import {ScrollEvent} from "ngx-scroll-event";
import {FileService} from "../../Services/FileService";
import {AccessType, UploadedFile} from "../../Entity/UploadedFile";
import {BasicResponse} from "../../Entity/BasicResponse";
import {StompService, StompState} from "@stomp/ng2-stompjs";
import {Message} from '@stomp/stompjs';
import {QuestionService} from "../../Services/QuestionService";
import {ComplaintService} from "../../Services/ComplaintService";
import {Question, ReportTopic} from "../../Entity/Question";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";

@Component({
  selector: 'app-chat-room',
  templateUrl: './chat-room.component.html',
  styleUrls: ['./chat-room.component.scss'],
  providers: [ChatMessageService, CacheService, FileService, QuestionService, ComplaintService]
})
export class ChatRoomComponent implements OnInit {
  page = 0;
  messages: Array<ChatMessage> = []
  user: User;
  baseUrl = AppContextUrl
  chatroomUid: string = ''
  showMoreBtn = true
  offset = 0;
  topic = ''
  grade = ''
  qId = 0
  qRate = 0;
  question: Question
  isStudent = true
  userType = ''
  reportTopicSelect: Array<any>
  reportTopic: ReportTopic;


  constructor(private snackBar: MatSnackBar , private complaintService: ComplaintService, private questionService: QuestionService, private stompService: StompService,private fileService: FileService,private chatMessageService: ChatMessageService, private activeRoute: ActivatedRoute , private cacheService: CacheService) {
    this.reportTopicSelect = ReportTopicSelect;
    this.activeRoute.params.subscribe(res => {
      this.chatroomUid = res['roomUid']
      this.getExistingMessages();
    })
    this.activeRoute.queryParams.subscribe(query => {

      this.topic = query['topic']
      this.grade = query['grade']
      this.qId = query['qId'];
    })
    this.user = this.cacheService.get('user');
    // if (this.cacheService.get('userType') == 'ROLE_TUTOR')
    // {
    //   this.isStudent = false
    // }
    this.userType = this.cacheService.get('userType');
    console.log(this.userType)
  }


  ngOnInit() {
    this.question = this.cacheService.get('current-q') as Question;
    console.log(this.user)
    this.stompService.state
      .subscribe((status: number) => {
        console.log(`Stomp connection status: ${StompState[status]}`);
      });


    this.stompService.subscribe('/topic/private.chat.' + this.chatroomUid).subscribe(mess =>{
      let chatMessage = JSON.parse(mess['body']) as ChatMessage
      if(chatMessage.authorUser.id != this.user.id)
      {
        this.messages.push(chatMessage);
        setTimeout(() => {
          document.body.querySelector('.messs').scrollTo({left: 0,top: document.body.querySelector('.messs').scrollHeight, behavior: 'smooth'});
        },200)
      }
    })
  }
  onSelectReport(event)
  {
    this.reportTopic = event.value;
    console.log(event);
  }
  onSendReport(desc, modal)
  {
    if (this.reportTopic != null)
    {
      this.complaintService.addComplaintReport(this.qId, desc, this.reportTopic).subscribe(res => {
        if ((res as BasicResponse).success)
        {
          this.openSnackBar('اعتراض با موفقیت ارسال شد.', 'success');
          modal.hide();
        }
        else
        {
          this.openSnackBar((res as BasicResponse).message, 'error');
        }
      })
    }
    else
    {
      this.openSnackBar('موضوع اعتراض را انتخاب کنید', 'error');
    }
  }

  onRateChange(event)
  {

    this.qRate = event.rating;
  }

  onEndQuestion(modal){
    if(this.qRate == 0)
    {
      this.openSnackBar('برای پایان جلسه به پاسخ دهنده امتیاز دهید.', 'error');
    }
    else
    {
      this.questionService.endQuestion(this.qId, this.qRate).subscribe(res => {
        if ((res as BasicResponse).success)
        {
          this.openSnackBar('جلسه با موفقیت به اتمام رسید', 'success');
          modal.hide();
        }
        else
        {
          this.openSnackBar((res as BasicResponse).message, 'error');
        }
      });
    }

  }

  getExistingMessages()
  {
     this.chatMessageService.getExistingChatMessages(this.chatroomUid, this.page).subscribe(mess => {
       console.log(mess);
       for(let m of mess)
       {
         this.messages.splice(0, 0, m);
       }
         setTimeout(() => {
             if (this.page == 0)
             {
                document.body.querySelector('.messs').scrollTo({left: 0,top: document.body.querySelector('.messs').scrollHeight, behavior: 'smooth'});
             }
             else
             {
                document.body.querySelector('.messs').scrollTop = document.body.querySelector('.messs').scrollHeight - this.offset;
             }
           },10)

       if((mess as Array<ChatMessage>).length >= 20)
       {
         this.showMoreBtn = true
       }
       else
       {
         this.showMoreBtn = false
       }

     })
  }

  onSendNewMessage(contents: string)
  {

    // messages.scrollIntoView()
    console.log('sdlkfjslkdjf')
    let chatMessage = new ChatMessage();
    chatMessage.contents = contents
    chatMessage.timeSent = new Date()
    chatMessage.authorUser = this.user;
    chatMessage.messageType = MessageType.SEND_TEXT;
    this.messages.push(chatMessage);

    this.chatMessageService.addChatMessage(chatMessage,this.chatroomUid).subscribe(res => {
      let bres = res as BasicResponse
      console.log(bres)
    })

    setTimeout(() => {
      document.body.querySelector('.messs').scrollTo({left: 0,top: document.body.querySelector('.messs').scrollHeight, behavior: 'smooth'});
    },200)
  }
  onShowMore()
  {
    this.offset = document.body.querySelector('.messs').scrollHeight - document.body.querySelector('.messs').scrollTop
    this.page ++;
    this.getExistingMessages();
  }

  onPickImage(event) {
    this.fileService.uploadFile(event.target.files[0], AccessType[AccessType.Public] , '').subscribe((response) => {
      if (response['success'] == true) {

        let uf = new UploadedFile();
        uf.id = response['message'];
        let chatMessage = new ChatMessage();
        chatMessage.mediaFile = uf;
        chatMessage.timeSent = new Date()
        chatMessage.authorUser = this.user;
        chatMessage.messageType = MessageType.SEND_IMAGE;
        this.chatMessageService.addChatMessage(chatMessage,this.chatroomUid).subscribe(res => {
          let bres = res as BasicResponse
          console.log(bres)
        })


        this.messages.push(chatMessage);
        setTimeout(() => {
          document.body.querySelector('.messs').scrollTo({left: 0,top: document.body.querySelector('.messs').scrollHeight, behavior: 'smooth'});
        },200)
      }
    });
  }


  onSupport()
  {
    console.log('sdfsdfsdf')
  }



  public handleScroll(event: ScrollEvent) {
    // if (event.isReachingBottom) {
    //   console.log(`the user is reaching the bottom`);
    // }
    if (event.isReachingTop) {

    }
    // if (event.isWindowEvent) {
    //   console.log(`This event is fired on Window not on an element.`);
    // }
  }


  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }

}
