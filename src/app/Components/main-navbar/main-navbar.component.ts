import {Component, Input, OnInit} from '@angular/core';
import {CacheService} from "ng2-cache-service";
import {Router} from "@angular/router";
import {User} from "../../Entity/User/User";
import {UserService} from "../../Services/UserService";

@Component({
  selector: 'app-main-navbar',
  templateUrl: './main-navbar.component.html',
  styleUrls: ['./main-navbar.component.scss'],
  providers:[UserService, CacheService]
})
export class MainNavbarComponent implements OnInit {

  @Input()
  fontColor = ''

  @Input()
  isFixed: boolean

  user : User
  userType = '';

  constructor(private cacheService: CacheService, private router: Router,private userService:UserService) {
    if(cacheService.exists('user'))
    {
      this.user = cacheService.get('user') as User;

      this.userType = cacheService.get('userType');
      console.log(this.userType)
    }
    // this.userService.getMyProfile().subscribe(user => {
    //   console.log(user)
    //   // if (user as User).role
    // })
  }



  ngOnInit() {

  }

  onLogout()
  {
    this.cacheService.removeAll();
    this.user = null
  }
}
