import { Component, OnInit } from '@angular/core';
import {PostService} from "../../../Services/PostService";
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";
import {Post} from "../../../Entity/Post";
import {FileContextUrl} from "../../../app.globals";
import fa from 'javascript-time-ago/locale/fa';
import {CacheService} from "ng2-cache-service";

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss'],
  providers: [PostService,CacheService]
})
export class PostListComponent implements OnInit {

  catid = 0
  posts: Array<Post> = []
  baseUrl = FileContextUrl;
  userType

  constructor(private router: Router, private cacheService: CacheService,private postService: PostService,private activatedRoute:ActivatedRoute) {
    this.userType = this.cacheService.get('userType')
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      if(params['catId'] != null)
      {
        this.catid = params['catId'];
        this.onShowPosts(this.catid);
      }
    })
  }

  onShowPosts(catId)
  {
    this.postService.getPostsByCategory(catId).subscribe(posts => {

      this.posts = posts;
    })
  }
  onShowPostDetails(post)
  {
    if(this.userType == 'ROLE_ADMIN' || this.userType == 'ROLE_EMPLOYEE')
    {
      let navigationExtras: NavigationExtras = {
        queryParams: { 'cat': this.catid},
      };
      this.router.navigate(['/employee/blog/posts/', post.id], navigationExtras);
    }
    else
    {
      this.router.navigate(['/blog/posts/', post.id]);
    }
  }
  
  onNewPost()
  {
    let navigationExtras: NavigationExtras = {
      queryParams: { 'cat': this.catid},
    };
    this.router.navigate(['/employee/blog/posts/new'], navigationExtras);
  }
  onDeletePost()
  {
    console.log('deleetteee')
  }

}
