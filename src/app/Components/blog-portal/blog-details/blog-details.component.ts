import { Component, OnInit } from '@angular/core';
import {Post} from "../../../Entity/Post";
import {PostService} from "../../../Services/PostService";
import {ActivatedRoute, Router} from "@angular/router";
import {FileContextUrl} from "../../../app.globals";
import {CacheService} from "ng2-cache-service";
import {FileService} from "../../../Services/FileService";
import {AccessType, UploadedFile} from "../../../Entity/UploadedFile";
import {post} from "selenium-webdriver/http";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";
import {BasicResponse} from "../../../Entity/BasicResponse";
import {Category} from "../../../Entity/Category";

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.scss'],
  providers: [PostService, FileService]
})
export class BlogDetailsComponent implements OnInit {

  post: Post;
  baseUrl = FileContextUrl;
  userType = 'ROLE_STUDENT';
  editable = false
  timeAgo
  spinner = false
  catId = 0

  constructor(private router:Router,private snackBar: MatSnackBar, private fileService: FileService , private postService: PostService, private activatedRoute: ActivatedRoute,private cacheService:CacheService) {

  }

  ngOnInit() {
    this.userType = this.cacheService.get('userType')

    if(this.userType == 'ROLE_EMPLOYEE' || this.userType == 'ROLE_ADMIN')
    {
      this.editable = true;
    }

    this.activatedRoute.params.subscribe(params => {
      if(params['id'] != null)
      {
        this.loadPost(params['id']);
      }
    })
    this.activatedRoute.queryParams.subscribe(params => {
      if(params['cat'] != null)
      {
        this.catId = params['cat'];
      }
    })

  }
  loadPost(id)
  {
    this.postService.getPostById(id).subscribe(res => {
      console.log(res)
      this.post = res
    })
  }

  onPickCatImg(event) {
    this.spinner = true
    this.fileService.uploadFile(event.target.files[0], AccessType[AccessType.Public] , '').subscribe((response) => {
      if (response['success'] == true) {
        if (this.post == null)
        {
          this.post = new Post();
        }
        let img = new UploadedFile()
        img.id = response['message'];
        this.post.coverImage = img;
        this.spinner = false
      }
    })
  }
  onNewPost(title, content)
  {
    if(this.post == null || this.post.coverImage == null)
    {
      this.openSnackBar('برای پست تصویری انتخاب نشده' , 'error')
      return null;
    }
    else if(title.length < 1)
    {
      this.openSnackBar('عنوانی برای این پست انتخاب نشده است' , 'error')
      return null;
    }
    else if(content.length < 1)
    {
      this.openSnackBar('محتوایی برای این پست انتخاب نشده است.' , 'error')
      return null;
    }

    this.post.title = title
    this.post.content = content
    let cat = new Category()
    cat.id = this.catId;
    this.post.category = cat
    this.post.publicationDate = new Date();

    if (this.post.id == null)
    {
      this.postService.addPost(this.post).subscribe(res => {
        let r = res as BasicResponse;
        if (r.success)
        {
          this.openSnackBar('پست شما با موفقیت ارسال شد.' , 'success');
          //this.router.navigate(['/employee/blog/cats']);
          window.history.back();
        }
        else
        {
          this.openSnackBar(r.message , 'error')
        }
      })
    }
    else
    {
      this.postService.updatePost(this.post).subscribe(res => {
        let r = res as BasicResponse;
        if (r.success)
        {
          this.openSnackBar('پست شما با موفقیت ویرایش شد.' , 'success');
         // this.router.navigate(['/employee/blog/cats']);
          window.history.back();
        }
        else
        {
          this.openSnackBar(r.message , 'error');
        }
      })
    }
  }


  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }

}


