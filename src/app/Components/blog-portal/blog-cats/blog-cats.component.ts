import { Component, OnInit } from '@angular/core';
import {CategoryService} from "../../../Services/CategoryService";
import {Category} from "../../../Entity/Category";
import {AppContextUrl, FileContextUrl} from "../../../app.globals";
import {PostService} from "../../../Services/PostService";
import {NavigationExtras, Router} from "@angular/router";
import {CacheService} from "ng2-cache-service";
import {FileService} from "../../../Services/FileService";
import {AccessType, UploadedFile} from "../../../Entity/UploadedFile";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";
import {BasicResponse} from "../../../Entity/BasicResponse";

@Component({
  selector: 'app-blog-cats',
  templateUrl: './blog-cats.component.html',
  styleUrls: ['./blog-cats.component.scss'],
  providers:[CategoryService, PostService, FileService,CacheService]
})
export class BlogCatsComponent implements OnInit {

  baseFile = FileContextUrl
  categories: Array<Category> = []
  userType = ''
  newCatImg: UploadedFile
  spinner = false

  constructor(private snackBar: MatSnackBar, private cacheService: CacheService, private router: Router, private postService: PostService, private categoryService: CategoryService, private fileService:FileService) {

  }

  ngOnInit() {
    this.userType = this.cacheService.get('userType')
    console.log(this.cacheService.get('userType'))
    this.loadCats()

  }

  loadCats()
  {
    this.categoryService.getCategories().subscribe(cats => {
      console.log(cats)
      this.categories = cats as Array<Category>;
    });
  }

  onShowPosts(catId)
  {
    let navigationExtras: NavigationExtras = {
      queryParams: { 'catId': catId},
    };
    this.router.navigate(['/blog/posts'], navigationExtras);
  }

  onPickCatImg(event) {
    this.spinner = true
    this.fileService.uploadFile(event.target.files[0], AccessType[AccessType.Public] , '').subscribe((response) => {
      if (response['success'] == true) {
        this.spinner = false
        this.newCatImg = new UploadedFile();
        this.newCatImg.id = response['message'];
      }
    })
  }
  onAddCat(modal , title)
  {
    if(this.newCatImg == null)
    {
      this.openSnackBar('تصویری برای دسته بندی انتخاب نشده' , 'error')
      return null;
    }
    else if(title == null || title.length < 1)
    {
      this.openSnackBar('موضوع دسته بندی انتخاب نشده' , 'error')
      return null;
    }
    let cat = new Category()
    cat.title = title
    cat.coverImage = this.newCatImg
    cat.categoryIndex = 0;
    this.categoryService.addCategory(cat).subscribe(res => {
      let r = res as BasicResponse
      if(r.success)
      {
        this.openSnackBar('دسته بندی با موفقیت اضافه شد' , 'success')
        this.loadCats()
        modal.hide();
      }
      else
      {
        this.openSnackBar(r.message , 'error');
      }
    })

  }

  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }
}
