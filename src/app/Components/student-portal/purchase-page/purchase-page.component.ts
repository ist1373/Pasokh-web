import { Component, OnInit } from '@angular/core';
import {PurchaseService} from "../../../Services/PurchaseService";
import {Purchase} from "../../../Entity/Purchase";
import {AppContextUrl, CreditScale, FileContextUrl} from "../../../app.globals";
import {TransactionService} from "../../../Services/TransactionService";
import {BasicResponse} from "../../../Entity/BasicResponse";
import {ActivatedRoute, Router} from "@angular/router";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";
import {User} from "../../../Entity/User/User";
import {UserService} from "../../../Services/UserService";
import {Student} from "../../../Entity/User/Student";
import {StudentService} from "../../../Services/StudentService";

@Component({
  selector: 'app-purchase-page',
  templateUrl: './purchase-page.component.html',
  styleUrls: ['./purchase-page.component.scss'],
  providers: [PurchaseService, TransactionService, UserService , StudentService]
})
export class PurchasePageComponent implements OnInit {

  baseFile = FileContextUrl
  availablePurchase: Array<Purchase>;
  credit = 0
  creditScale = CreditScale
  spinner = false

  constructor(private studentService: StudentService, private userService: UserService, private router: Router ,private purchaseService: PurchaseService, private transactionService: TransactionService,private ativatedRoute: ActivatedRoute,private snackBar:MatSnackBar) { }

  ngOnInit() {

    this.loadAvailablePurchase();
    this.getUser();
    this.ativatedRoute.queryParams.subscribe(params => {

      if (params['Status'] == 'NOK')
      {
        this.openSnackBar('عملیات خرید موفقیت آمیز نبود', 'error')
      }
      else if (params['Status'] == 'OK')
      {
        this.transactionService.zarrinPaymentVerification(params['pId'], params['Authority']).subscribe(res =>{
          let bres = res as BasicResponse;
          console.log(bres)
          this.openSnackBar('عملیات خرید موفقیت آمیز بود', 'success');
          this.getUser();
        //  this.loadUser();
        })
      }
      this.router.navigate([], {queryParams: null});
    })
  }
  getUser()
  {
    this.spinner = true
    this.userService.getMyProfile().subscribe(user => {
      console.log(user)
      this.credit = (user as Student).credit;
      this.spinner = false;
    })
  }
  // loadUser()
  // {
  //   this.userService.getMyProfile().subscribe(user => {
  //
  //   })
  // }

  loadAvailablePurchase()
  {
    this.spinner = true
    this.purchaseService.getAvailablePurchase().subscribe(perchases => {
      console.log(perchases)
      this.availablePurchase = perchases as Array<Purchase>;
      this.spinner = false
    })
  }
  onPurchaseBuy(purchase)
  {
    this.spinner = true
    this.transactionService.zarrinPaymentRequest(purchase).subscribe(res => {
      let bres = res as BasicResponse;
      console.log(bres);
      window.open('https://www.zarinpal.com/pg/StartPay/' + bres.message, '_blank');
      this.spinner = false
    })
  }

  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }

}
