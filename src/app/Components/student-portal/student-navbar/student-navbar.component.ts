import {Component, Input, OnInit} from '@angular/core';
import {CacheService} from "ng2-cache-service";
import {Router} from "@angular/router";
import {UserService} from "../../../Services/UserService";
import {CreditScale} from "../../../app.globals";

@Component({
  selector: 'app-student-navbar',
  templateUrl: './student-navbar.component.html',
  styleUrls: ['./student-navbar.component.scss'],
  providers:[UserService,CacheService]
})
export class StudentNavbarComponent implements OnInit {

  @Input()
  fontColor = ''

  @Input()
  background = '#fafafa!important'

  @Input()
  fabButton = true



  constructor(private userService: UserService, private cacheService: CacheService , private router: Router) { }

  ngOnInit() {

  }

  logout()
  {
     this.cacheService.removeAll();
     this.router.navigate(['/signIn']);
  }

}
