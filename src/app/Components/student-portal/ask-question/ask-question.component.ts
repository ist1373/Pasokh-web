import { Component, OnInit } from '@angular/core';
import {shakeAnimation, slideInOutAnimation} from "../../../app.animations";
import {FileService} from "../../../Services/FileService";
import {AccessType, UploadedFile} from "../../../Entity/UploadedFile";
import {AppContextUrl} from "../../../app.globals";
import {Grade, Question, QuestionTopic} from "../../../Entity/Question";
import {QuestionService} from "../../../Services/QuestionService";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";
import {UserService} from "../../../Services/UserService";
import {User} from "../../../Entity/User/User";
import {CacheService} from "ng2-cache-service";
import {BasicResponse} from "../../../Entity/BasicResponse";
import {Router} from "@angular/router";
import {Tutor} from "../../../Entity/User/Tutor";
import {TutorService} from "../../../Services/TutorService";

@Component({
  selector: 'app-ask-question',
  templateUrl: './ask-question.component.html',
  styleUrls: ['./ask-question.component.scss'],
  animations: [shakeAnimation, slideInOutAnimation],
  providers: [FileService, QuestionService, UserService , CacheService , TutorService]
})
export class AskQuestionComponent implements OnInit {

  questionImage: UploadedFile;
  baseUrl = AppContextUrl;
  user: User
  spinner = false
  tutorType = 'public'
  responderId = 0;

  tutors: Array<Tutor> = []

  constructor(private tutorService:TutorService , private router:Router, private cacheService: CacheService, private fileService: FileService, private questionService: QuestionService, private snackBar: MatSnackBar, private userService:UserService) { }

  ngOnInit() {
    this.user = this.cacheService.get('user') as User;
  }

  onPickImage(event) {
    this.spinner = true
    this.fileService.uploadFile(event.target.files[0], AccessType[AccessType.Public] , '').subscribe((response) => {
      if (response['success'] == true) {
        this.questionImage = new UploadedFile();
        this.questionImage.id = response['message'];

      }
      this.spinner = false
    });
  }

  onChangeTutorType(event) {
    this.tutorType = event.value;
    if(event.value == 'direct')
    {
      this.getFavorateTutor()
    }
  }

  onSaveQuestion(grade: string, topic: string, desc: string)
  {

    if (grade == null)
    {
      this.openSnackBar('لطفا مقطع تحصیلی خود را انتخاب کنید' , 'error')
    }
    else if (this.responderId == 0 && this.tutorType == 'direct')
    {
      this.openSnackBar('لطفا پاسخ دهنده خود را انتخاب کنید' , 'error')
    }
    else if( topic == null)
    {
      this.openSnackBar('لطفا موضوع سوال خود را انتخاب کنید' , 'error')
    }
    else if (desc.length < 10)
    {
      this.openSnackBar('لطفا توضیح سوال خود را تکمیل کنید(حداقل ۱۰ کاراکتر)' , 'error')
    }
    else if (this.questionImage == null)
    {
      this.openSnackBar('لطفا عکس سوال خود را بفرستید' , 'error')
    }
    else {
      this.spinner = true;
      let question = new Question()
      question.grade = Grade[grade];

      if (this.responderId != 0 && this.tutorType == 'direct')
      {
        let responder = new User()
        responder.id = this.responderId
        question.responder = responder;
      }

      question.questionTopic = QuestionTopic[topic];
      question.description = desc;
      question.image = this.questionImage;
      question.questioner = this.user
      this.questionService.addQuestion(question).subscribe(res => {
        let bres = res as BasicResponse;
        if (bres.success)
        {
          this.router.navigate(['/student/questionList']);
        }
        else
        {
          this.openSnackBar( bres.message , 'error');
        }
        this.spinner = false;
      })
    }
  }

  getFavorateTutor()
  {
    this.spinner = true
    this.tutorService.getFavoriteTutorByStudent().subscribe(tutors => {
      this.tutors = tutors;
      this.spinner = false
      console.log(tutors);
    })

  }

  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }


}
