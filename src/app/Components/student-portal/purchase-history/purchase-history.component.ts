import { Component, OnInit } from '@angular/core';
import {Transaction} from "../../../Entity/Transaction";
import {TransactionService} from "../../../Services/TransactionService";
import {ScrollEvent} from "ngx-scroll-event";

@Component({
  selector: 'app-purchase-history',
  templateUrl: './purchase-history.component.html',
  styleUrls: ['./purchase-history.component.scss'],
  providers: [TransactionService]
})
export class PurchaseHistoryComponent implements OnInit {
  page = 0;
  isLoadingData = false
  spinner = false
  transactions: Array<Transaction> = []

  constructor(private transactionService: TransactionService) { }

  ngOnInit() {
    this.loadPurchases();
  }

  loadPurchases()
  {
    this.isLoadingData = true
    this.spinner = true
    this.transactionService.getTransactionByPayer(this.page).subscribe(res => {
      for (let r of res)  {
        if(this.page == 0)
        {
          this.transactions.push(r);
        }
        else {
          for(let p of res)
          {
            this.transactions.push(p)
          }
        }

      }
      this.spinner = false
      if(res.length > 0)
      {
        this.isLoadingData = false
      }
    })
  }

  public handleScroll(event: ScrollEvent) {
    if (event.isReachingBottom) {
      if (!this.isLoadingData)
      {
        this.page += 1
        this.loadPurchases()
        console.log(`the user is reaching the bottom`);
      }
    }
  }

}
