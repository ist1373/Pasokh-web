import {Component, Input, OnInit} from '@angular/core';
import {QuestionService} from "../../../Services/QuestionService";
import {Question, QuestionState} from "../../../Entity/Question";
import {AppContextUrl, CreditScale} from "../../../app.globals";
import {NavigationExtras, Router} from "@angular/router";
import {UserService} from "../../../Services/UserService";
import {Student} from "../../../Entity/User/Student";
import {CacheService} from "ng2-cache-service";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";
import {ScrollEvent} from "ngx-scroll-event";


@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.scss'],
  animations:[],
  providers: [QuestionService, UserService,CacheService]
})
export class QuestionListComponent implements OnInit {

  mess = 'شما هنوز سوالی نپرسیده اید'
  questions: Array<Question> = []
  date = new Date();
  page = 0;
  spinner = false

  credit = 0;
  creditScale = CreditScale

  baseUrl = AppContextUrl

  waitingQuestions = true

  timeAgo

  isLoadingData = false

  constructor(private snackBar: MatSnackBar, private cacheService: CacheService, private userService: UserService, private questionService: QuestionService, private router: Router) {

  }

  ngOnInit() {
    if(this.cacheService.get('questionsType') == 'accepted') {
      this.waitingQuestions = false;
    }
    else {
      this.waitingQuestions = true;
    }

    this.getQuestion();
    this.getUser()
  }

  getQuestion()
  {
    this.isLoadingData = true
    this.spinner = true
    if (this.waitingQuestions)
    {
      this.questionService.getUserQuestionsByState([QuestionState.Pending] , this.page).subscribe(res => { //TODO add more
        console.log(res);
        if (this.page == 0){
          this.questions = res as Array<Question>;
        }
        else {
          for(let q of res)
          {
            this.questions.push(q);
          }
        }
        if ((res as Array<Question>).length > 0)
        {
          this.isLoadingData = false
        }
        this.spinner = false
      })
    }
    else
    {
      this.questionService.getUserQuestionsByState([QuestionState.Accepted, QuestionState.Finished] , this.page).subscribe(res => { //TODO add more

        if (this.page == 0){
          this.questions = res as Array<Question>;
        }
        else {
          for(let q of res)
          {
            this.questions.push(q);
          }
        }
        this.spinner = false


        if ((res as Array<Question>).length > 0)
        {
          this.isLoadingData = false
        }
      })
    }

  }

  getUser()
  {
    this.spinner = true
    this.userService.getMyProfile().subscribe(user => {
      console.log(user)
      this.credit = (user as Student).credit;
      this.spinner = false;
    })
  }

  onClickQuestion(q: Question)
  {
    if(q.questionState == 'Accepted' || q.questionState == 'Finished')
    {
      this.cacheService.set('current-q', q);
      let navigationExtras: NavigationExtras = {
        queryParams: { 'grade': q.grade, 'topic': q.questionTopic , 'qId': q.id},
      };

      this.router.navigate(['/student/chatRoom/' + q.chatRoom.uid],navigationExtras);
    }
    else if (q.questionState == 'Pending')
    {
        this.openSnackBar('سوال شما در صف انتظار است.لطفا منتظر بمانید...' , 'success');
    }

  }

  onSelectCredits()
  {
    this.router.navigate(['/student/purchasePage']);
  }
  onSelectWaitingQuestions()
  {
    this.waitingQuestions = true;
    this.page = 0
    this.getQuestion()
    this.cacheService.set('questionsType', 'waiting');
  }
  onSelectAcceptedQuestions()
  {
    this.waitingQuestions = false;
    this.page = 0
    this.getQuestion()
    this.cacheService.set('questionsType', 'accepted');
  }

  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }

  public handleScroll(event: ScrollEvent) {
    if (event.isReachingBottom) {
      if (!this.isLoadingData)
      {
        this.page += 1
        this.getQuestion()
        console.log(`the user is reaching the bottom`);
      }
    }
  }

}


