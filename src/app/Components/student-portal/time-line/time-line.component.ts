import { Component, OnInit } from '@angular/core';
import {TimeLine} from "../../../Entity/TimeLine";
import {TimeLineService} from "../../../Services/TimeLineService";
import {ActivatedRoute} from "@angular/router";
import {ScrollEvent} from "ngx-scroll-event";

@Component({
  selector: 'app-time-line',
  templateUrl: './time-line.component.html',
  styleUrls: ['./time-line.component.scss'],
  providers: [TimeLineService]
})
export class TimeLineComponent implements OnInit {



  page = 0
  size = 20
  isLoadingData = false

  timelines: Array<TimeLine> = []

  forStudent = true
  spinner = false

  timeAgo;

  constructor(private timelineService: TimeLineService, private activeRoute: ActivatedRoute) {

  }

  ngOnInit() {

    this.activeRoute.url.subscribe(urls => {
      if (urls[0].path == 'tutor')
      {
        this.forStudent = false
      }
    })
    this.loadTimeline()

  }

  loadTimeline()
  {
    this.spinner = true
    this.isLoadingData = true
    this.timelineService.getTimeLines(this.page, this.size).subscribe(res => {
        console.log(res)
        this.spinner = false
        if(this.page == 0) {
          this.timelines = res as Array<TimeLine>
        }
        else {
          for(let t of res)
          {
            this.timelines.push(t)
          }
        }

        if((res as Array<TimeLine>).length > 0)
        {
          this.isLoadingData = false
        }
      },
      error => {
      })
  }

  public handleScroll(event: ScrollEvent) {
    if (event.isReachingBottom) {
      if (!this.isLoadingData)
      {
        this.page += 1
        this.loadTimeline()
        console.log(`the user is reaching the bottom`);
      }
    }
  }

}
