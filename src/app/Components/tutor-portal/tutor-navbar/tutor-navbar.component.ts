import {Component, Input, OnInit} from '@angular/core';
import {CacheService} from "ng2-cache-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-tutor-navbar',
  templateUrl: './tutor-navbar.component.html',
  styleUrls: ['./tutor-navbar.component.scss']
})
export class TutorNavbarComponent implements OnInit {

  @Input()
  fontColor = ''

  @Input()
  background = '#fafafa!important'

  @Input()
  title = 'پنل کاربری پاسخگو'


  constructor(private cacheService:CacheService,private router:Router) { }

  ngOnInit() {
  }

  logout()
  {
    this.cacheService.removeAll();
    this.router.navigate(['/signIn']);
  }
}
