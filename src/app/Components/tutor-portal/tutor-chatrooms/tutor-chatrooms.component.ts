import { Component, OnInit } from '@angular/core';
import {QuestionService} from "../../../Services/QuestionService";
import {Question, QuestionState} from "../../../Entity/Question";
import {AppContextUrl} from "../../../app.globals";

import {NavigationExtras, Router} from "@angular/router";
import {CacheService} from "ng2-cache-service";
import {ScrollEvent} from "ngx-scroll-event";

@Component({
  selector: 'app-tutor-chatrooms',
  templateUrl: './tutor-chatrooms.component.html',
  styleUrls: ['./tutor-chatrooms.component.scss'],
  providers:[QuestionService]
})
export class TutorChatroomsComponent implements OnInit {

  spinner = false
  questions: Array<Question> = [];
  page = 0;
  baseUrl = AppContextUrl
  isLoadingData = false

  constructor(private router: Router, private questionService: QuestionService, private cacheService:CacheService) {

  }

  ngOnInit() {
    this.getChatRooms();
  }

  getChatRooms()
  {
    this.isLoadingData = true
    this.spinner = true
    this.questionService.getActiveQuestionsByTutor(this.page).subscribe(qs => { //TODO add more
      let nq = qs as Array<Question>
      if (this.page == 0)
      {
        this.questions = nq;
      }
      else
      {
        for (let q of nq)
        {
          this.questions.push(q);
        }
      }
      if(qs.length > 0)
      {
        this.isLoadingData = false
      }
      this.spinner = false;
    })
  }

  public handleScroll(event: ScrollEvent) {
    if (event.isReachingBottom) {
      if (!this.isLoadingData)
      {
        this.page += 1
        this.getChatRooms()
        console.log(`the user is reaching the bottom`);
      }
    }
  }

  onOpenChatRoom(q)
  {
    this.cacheService.set('current-q', q);
    let navigationExtras: NavigationExtras = {
      queryParams: { 'grade': q.grade, 'topic': q.questionTopic, 'qId': q.id},
    };
    this.router.navigate(['/student/chatRoom/' + q.chatRoom.uid], navigationExtras);

  }
}
