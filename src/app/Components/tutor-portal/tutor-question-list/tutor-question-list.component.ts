import { Component, OnInit } from '@angular/core';
import {Question, QuestionState} from "../../../Entity/Question";
import {AppContextUrl, CreditValue, ReportTypeSelect} from "../../../app.globals";
import {ActivatedRoute, NavigationExtras, Router} from "@angular/router";
import {UserService} from "../../../Services/UserService";
import {QuestionService} from "../../../Services/QuestionService";
import {MatSnackBar, MatSnackBarConfig, ThemePalette} from "@angular/material";
import {BidService} from "../../../Services/BidService";
import {BasicResponse} from "../../../Entity/BasicResponse";
import {ComplaintService} from "../../../Services/ComplaintService";
import {ReportState} from "../../../Entity/ComplaintReport";
import {QuestionReportService} from "../../../Services/QuestionReportService";
import {SettlementService} from "../../../Services/SettlementService";
import {CacheService} from "ng2-cache-service";
import {Tutor} from "../../../Entity/User/Tutor";
import {ScrollEvent} from "ngx-scroll-event";

@Component({
  selector: 'app-tutor-question-list',
  templateUrl: './tutor-question-list.component.html',
  styleUrls: ['./tutor-question-list.component.scss'],
  providers:[UserService, QuestionService, BidService, QuestionReportService, SettlementService,CacheService]
})
export class TutorQuestionListComponent implements OnInit {

  reportTypeSelect: Array<any>
  questions: Array<Question> = []
  date = new Date();
  page = 0;
  spinner = false
  actionType: string

  qIdReport = 0;
  topicReport;

  credit = 0;

  detailedQuestion = -1;

  mess = 'سوال مرتبطی وجود ندارد'

  baseUrl = AppContextUrl

  directQuestions = false
  creditValue = CreditValue;
  availableMoney = 0;
  avgStar = 0;
  isLoadingData = false

  rotateDeg = 0;


  constructor(private activeRoute: ActivatedRoute, private cacheService: CacheService, private settlementService: SettlementService, private questionReportService: QuestionReportService, private snackBar: MatSnackBar, private userService: UserService, private questionService: QuestionService, private router: Router, private bidService:BidService) {


    this.reportTypeSelect = ReportTypeSelect;

  }

  ngOnInit() {

    this.getAvailableMony();
    this.getTutorRate();
    if(this.cacheService.get('questionsType') == 'public')
    {
      this.directQuestions = false
    }
    else if(this.cacheService.get('questionsType') == 'direct')
    {
      this.directQuestions = true
    }
    this.getQuestion();
  }

  getQuestion()
  {

    this.spinner = true
    this.isLoadingData = true

    if (this.directQuestions)
    {
      this.questionService.getDirectQuestions(this.page).subscribe(res => { //TODO add more
        console.log(res);
        if(this.page == 0) {
          this.questions = res as Array<Question>;
        }
        else
        {
          for(let q of res)
          {
            this.questions.push(q)
          }
        }
        if(res.length > 0)
        {
          this.isLoadingData = false
        }
        this.spinner = false;
      }, error => {
        console.log(error)
        this.spinner = false;
        let er = error.error as BasicResponse
        this.mess = er.message
      })
    }
    else
    {
      this.questionService.getPublicQuestions(this.page).subscribe(res => { //TODO add more
        console.log(res);
        if(this.page == 0) {
          this.questions = res as Array<Question>;
        }
        else
        {
          for(let q of res)
          {
            this.questions.push(q)
          }
        }
        if(res.length > 0)
        {
          this.isLoadingData = false
        }
        this.spinner = false;
      }, error => {
        console.log(error.error)
        let er = error.error as BasicResponse
        this.mess = er.message
        this.spinner = false;
      })
    }
  }

  getTutorRank() {
     // this.userService.getMyProfile().subscribe(user => { //TODO getRank
     //   this.tutorRank = (user as Tutor).ran
     // })
  }

  getAvailableMony()
  {

    this.settlementService.getAvailableMoney().subscribe(res => {
      this.availableMoney = Number.parseInt((res as BasicResponse).message)
      console.log(res);
    })
  }
  getTutorRate()
  {
    this.userService.getMyProfile().subscribe(res => {
      console.log(res)
      this.avgStar = (res as Tutor).avgStar;
    })
  }




  onClickQuestion(q: Question)
  {
    let navigationExtras: NavigationExtras = {
      queryParams: { 'grade': q.grade, 'topic': q.questionTopic},
    };
    this.router.navigate(['/student/chatRoom/' + q.chatRoom.uid],navigationExtras);
  }

  onSelectReport(event,q)
  {
    this.topicReport = event.value;
    this.qIdReport = q.id;
  }

  // onChangeBid(event,q)
  // {
  //
  //   console.log(event)
  //   console.log(this.actionType)
  //   console.log(q)
  // }

  onShowDetails(i,qq) {
    // document.body.querySelector(qq).scrollIntoView()
    // window.scrollTo({left: 0,top: document.body.querySelector(qq).scrollTop = 0 , behavior: 'smooth'});


    if (this.detailedQuestion == -1 || i < this.detailedQuestion)
    {
      window.scrollTo({
        'behavior': 'smooth',
        'left': 0,
        'top': document.body.querySelector(qq).offsetTop - 100
      });

      this.detailedQuestion = i;
      this.rotateDeg = 0;

    }
    else if (i > this.detailedQuestion)
    {
      window.scrollTo({
        'behavior': 'smooth',
        'left': 0,
        'top': document.body.querySelector(qq).offsetTop - 500
      });

      this.detailedQuestion = i;
      this.rotateDeg = 0;
    }


  }
  onRotate() {
    if (this.rotateDeg < 270)
    {
      this.rotateDeg += 90;
    }
    else {
      this.rotateDeg = 0;
    }
  }

  onSelectPublicQuestions()
  {
    this.questions = [];
    this.directQuestions = false;
    this.page = 0
    this.getQuestion()
    this.cacheService.set('questionsType', 'public');

  }

  onSelectDirectQuestions()
  {
    this.questions = [];
    this.directQuestions = true;
    this.page = 0
    this.getQuestion()
    this.cacheService.set('questionsType', 'direct');

  }

  onBid(index, q: Question, amount)
  {
    this.spinner = true
    console.log(amount/8)
    this.bidService.addBid(q.id, amount/8).subscribe(res => {
      console.log(res)
      if ((res as BasicResponse).success)
      {
        this.openSnackBar('پیشنهاد شما با موفقیت ارسال شد.' , 'success')
        setTimeout(() => {
          this.questions.splice(index, 1)
        }, 1000)
        this.detailedQuestion = -1;
        this.spinner = false;
      }
      else
      {
        this.openSnackBar((res as BasicResponse).message , 'error');
      }

    });

  }
  onReport(q, desc, index)
  {
    if (q.id == this.qIdReport)
    {
      this.spinner = true
      this.questionReportService.addReport(q.id, desc, this.topicReport).subscribe(res => {
        console.log(res)
        if ((res as BasicResponse).success)
        {
          this.detailedQuestion = -1;
          setTimeout(() => {
            this.questions.splice(index, 1)
          }, 1000)
        }
        else
        {
          this.openSnackBar((res as BasicResponse).message , 'error');
        }
        this.spinner = false
      })
    }
    else
    {
      this.openSnackBar('برای این سوال گزارشی ثبت نشده' , 'error');
    }

  }
  onAccept(q)
  {
    this.spinner = true
    this.bidService.addDirectBid(q.id, true).subscribe(res => {
      if ((res as BasicResponse).success)
      {
        console.log((res as BasicResponse).data)
        this.detailedQuestion = -1;
        this.spinner = false;
        this.cacheService.set('current-q', q);
        let navigationExtras: NavigationExtras = {
          queryParams: { 'grade': q.grade, 'topic': q.questionTopic, 'qId': q.id},
        };
        this.router.navigate(['/tutor' , 'chatRoom' , (res as BasicResponse).data], navigationExtras);
      }
      else
      {
        this.openSnackBar((res as BasicResponse).message , 'error');
      }
      this.spinner = false
    })
  }
  onReject(q,i)
  {
    this.spinner = true
    this.bidService.addDirectBid(q.id, false).subscribe(res => {
      if ((res as BasicResponse).success)
      {
        console.log((res as BasicResponse).data)
        this.detailedQuestion = -1;
        this.spinner = false;
        setTimeout(() => {
          this.questions.splice(i, 1)
        }, 1000)
      }
      else
      {
        this.openSnackBar((res as BasicResponse).message , 'error');
      }
      this.spinner = false
    })
  }

  public handleScroll(event: ScrollEvent) {
    if (event.isReachingBottom) {
      if (!this.isLoadingData)
      {
        this.page += 1
        this.getQuestion()
        console.log(`the user is reaching the bottom`);
      }
    }
  }


  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }
}
