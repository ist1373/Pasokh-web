import {Component, HostListener, OnInit} from '@angular/core';
import {ScrollEvent} from "ngx-scroll-event";
import {Tutor} from "../../../../Entity/User/Tutor";
import {AccessType, UploadedFile} from "../../../../Entity/UploadedFile";
import {FileContextUrl} from "../../../../app.globals";
import {TutorService} from "../../../../Services/TutorService";
import {UserService} from "../../../../Services/UserService";
import {FileService} from "../../../../Services/FileService";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";
import {BasicResponse} from "../../../../Entity/BasicResponse";

@Component({
  selector: 'app-financial-account',
  templateUrl: './financial-account.component.html',
  styleUrls: ['./financial-account.component.scss'],
  providers: [UserService, FileService, TutorService]
})
export class FinancialAccountComponent implements OnInit {
  spinner = false;
  isFixed = false;

  tutor: Tutor;
  nationalImg: UploadedFile;
  baseUrl = FileContextUrl;


  constructor(private tutorService: TutorService, private userService: UserService,private fileService: FileService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getUser()
  }

  getUser(){
    this.userService.getMyProfile().subscribe(res => {
      console.log(res)
      this.tutor = res as Tutor
      if (this.tutor.nationalCardImage != null)
      {
        this.nationalImg = this.tutor.nationalCardImage;
      }
    })
  }

  onPickNationalImg(event){
    this.fileService.uploadFile(event.target.files[0], AccessType[AccessType.Public] , '').subscribe((response) => {
      if (response['success'] == true) {
        this.nationalImg = new UploadedFile();
        this.nationalImg.id = response['message'];
      }
    })
  }
  onEdit(postalcode, shabacode, nationalcode, address)
  {
    if (postalcode != null)
      this.tutor.postalCode = postalcode;
    if (shabacode != null)
      this.tutor.shabaCode = shabacode;
    if (nationalcode != null)
      this.tutor.nationalCode = nationalcode
    if (address != null)
      this.tutor.address = address
    if (this.nationalImg != null)
      this.tutor.nationalCardImage = this.nationalImg;
    this.saveTutor()
  }

  saveTutor()
  {
    this.spinner = true
    this.tutorService.editTutor(this.tutor.id,this.tutor).subscribe(res => {
      if ((res as BasicResponse).success)
      {
        this.openSnackBar('تغییرات با موفقیت ذخیره شد.' , 'success');
      }
      else
      {
        this.openSnackBar((res as BasicResponse).message , 'error');
      }
      this.spinner = false;
    })
  }

  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }


}
