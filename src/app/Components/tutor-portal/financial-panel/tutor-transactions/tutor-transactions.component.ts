import { Component, OnInit } from '@angular/core';
import {QuestionService} from "../../../../Services/QuestionService";
import {EndedQuestion} from "../../../../Entity/EndedQuestion";

import {CreditValue} from "../../../../app.globals";
import {ScrollEvent} from "ngx-scroll-event";


@Component({
  selector: 'app-tutor-transactions',
  templateUrl: './tutor-transactions.component.html',
  styleUrls: ['./tutor-transactions.component.scss'],
  providers: [QuestionService]
})
export class TutorTransactionsComponent implements OnInit {

  page = 0;
  timeAgo;
  spinner = false
  isLoadingData = false
  creditValue = CreditValue
  endedQuestions: Array<EndedQuestion> = []

  constructor(private questionService: QuestionService) {

  }

  ngOnInit() {
    this.getEndedQuestions();
  }

  getEndedQuestions()
  {
    this.spinner = true
    this.isLoadingData = true
    this.questionService.getEndedQuestionByResponder(this.page).subscribe(res => {
      console.log(res)
      if(this.page == 0){
        this.endedQuestions = res;
      }
      else {
        for(let q of res)
        {
          this.endedQuestions.push(q);
        }
      }
      this.spinner = false
      if(res.length > 0)
      {
        this.isLoadingData = false;
      }
    })
  }

  public handleScroll(event: ScrollEvent) {
    if (event.isReachingBottom) {
      if (!this.isLoadingData)
      {
        this.page += 1
        this.getEndedQuestions()
        console.log(`the user is reaching the bottom`);
      }
    }
  }
}
