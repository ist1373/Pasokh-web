import { Component, OnInit } from '@angular/core';
import {SettlementService} from "../../../../Services/SettlementService";
import {BasicResponse} from "../../../../Entity/BasicResponse";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";
import {Settlement} from "../../../../Entity/Settlement";
import {ScrollEvent} from "ngx-scroll-event";

@Component({
  selector: 'app-tutor-payments',
  templateUrl: './tutor-payments.component.html',
  styleUrls: ['./tutor-payments.component.scss'],
  providers:[SettlementService]
})
export class TutorPaymentsComponent implements OnInit {

  availableMoney = 0;
  settlements: Array<Settlement> = [];
  isLoadingData = false
  spinner = false
  page = 0;

  constructor(private settlementService: SettlementService , private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.getAvailableMoney();
    this.getSettlemtns();

  }

  requestPayment()
  {
    this.settlementService.addSettlement().subscribe(res => {
      if ((res as BasicResponse).success)
      {
        this.openSnackBar('درخواست شما با موفقیت ارسال شد' , 'success')
      }
      else
      {
        this.openSnackBar( (res as BasicResponse).message, 'error')
      }
    })
  }

  getSettlemtns()
  {
    this.spinner = true
    this.isLoadingData = true
    this.settlementService.getSettlementsByTutor(this.page).subscribe(res => {
      let setts = res as Array<Settlement>
      if(this.page == 0)
      {
        this.settlements = setts
      }
      else
      {
        for(let set of setts)
        {
          this.settlements.push(set)
        }
      }
      if(res.length > 0)
      {
        this.isLoadingData = false
      }
      this.spinner = false
      console.log(this.settlements)
    })
  }

  getAvailableMoney()
  {
    this.settlementService.getAvailableMoney().subscribe(res => {
      this.availableMoney = Number.parseInt((res as BasicResponse).message);
    })
  }

  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }

  public handleScroll(event: ScrollEvent) {
    if (event.isReachingBottom) {
      if (!this.isLoadingData)
      {
        this.page += 1
        this.getSettlemtns()
        console.log(`the user is reaching the bottom`);
      }
    }
  }

}
