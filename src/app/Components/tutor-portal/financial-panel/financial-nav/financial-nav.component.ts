import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-financial-nav',
  templateUrl: './financial-nav.component.html',
  styleUrls: ['./financial-nav.component.scss']
})
export class FinancialNavComponent implements OnInit {

  @Input()
  fontColor = ''

  @Input()
  background = '#fafafa!important'

  @Input()
  title = 'پنل کاربری پاسخگو'


  constructor() { }

  ngOnInit() {
  }

}
