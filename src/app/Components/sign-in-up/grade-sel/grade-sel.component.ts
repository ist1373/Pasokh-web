import { Component, OnInit } from '@angular/core';
import {shakeAnimation, slideInOutAnimation} from "../../../app.animations";
import {Router} from "@angular/router";

@Component({
  selector: 'app-grade-sel',
  templateUrl: './grade-sel.component.html',
  styleUrls: ['./grade-sel.component.scss'],
  animations:[shakeAnimation, slideInOutAnimation],
  providers:[]
})
export class GradeSelComponent implements OnInit {
  shakable = 'fix'
  constructor(private router:Router) { }

  ngOnInit() {
  }

  showStudentPortal(){
    this.router.navigate(['/student/questionList']);
  }
}
