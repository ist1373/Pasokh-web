import { Component, OnInit } from '@angular/core';
import {shakeAnimation, slideInOutAnimation} from "../../../app.animations";
import {UserService} from "../../../Services/UserService";
import { Router} from "@angular/router";

@Component({
  selector: 'app-forgot-pass',
  templateUrl: './forgot-pass.component.html',
  styleUrls: ['./forgot-pass.component.scss'],
  animations: [shakeAnimation, slideInOutAnimation],
  providers: [UserService]

})
export class ForgotPassComponent implements OnInit {
  shakable = 'fix'

  constructor(private userService: UserService, private router: Router) {

  }

  ngOnInit() {

  }

  onForgot(empho: string)
  {
    this.userService.forgotPass(empho).subscribe(res => {
      console.log(res)
    })
  }

}
