import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {shakeAnimation, slideInOutAnimation} from "../../../app.animations";
import {StudentService} from "../../../Services/StudentService";
import {StudentDto} from "../../../Entity/DTO/StudentDto";
import {TutorService} from "../../../Services/TutorService";
import {TutorDto} from "../../../Entity/DTO/TutorDto";
import {isUndefined} from "util";
import {BasicResponse} from "../../../Entity/BasicResponse";
import {CacheService} from "ng2-cache-service";

@Component({
  selector: 'app-student-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
  animations: [slideInOutAnimation, shakeAnimation],
  providers: [StudentService, TutorService, CacheService]
})
export class SignUpComponent implements OnInit {
  roleSelection: number
  animVar = 'in'

  errorMessage = ''
  hasError = false;
  shakable = 'fix';

  public mask = [ /[0]/ , /[9]/ , /[0-3]/, /\d/, /\d/, /\d/, /\d/ , /\d/, /\d/, /\d/, /\d/]

  constructor(private _cacheService: CacheService,private router: Router, private studentService: StudentService, private tutorService: TutorService) { }

  ngOnInit() {
  }

  goToSignIn() {
    this.router.navigate(['signIn']);
  }

  onSignUp(phone: string, email: string, password: string) {

    console.log(this.roleSelection)

    if (isUndefined(this.roleSelection)) {
      this.hasError = true;
      this.errorMessage = 'نوع فعالیت شما انتخاب نشده است'
      this.shakable = 'shake';
      setTimeout(() => {
        this.shakable = 'fix';
      }, 500);
    }
    else if (this.roleSelection === 1) {
      const studentDto = new StudentDto();
      studentDto.password = password
      studentDto.email = email
      studentDto.mobileNumber = phone
      this.studentService.registerStudent(studentDto).subscribe(res => {
        console.log(res);
        this._cacheService.set('phoneNumber', phone);
        this._cacheService.set('userType', 'ROLE_STUDENT');
        this.router.navigate(['verification']);
      }, (error) => {
        this.hasError = true;
        console.log(error)

        this.errorMessage = (error.error as BasicResponse).message
        this.shakable = 'shake';
        setTimeout(() => {
          this.shakable = 'fix';
        }, 500);
      });
    }
    else if (this.roleSelection === 2) {
      const tutorDto = new TutorDto();
      tutorDto.password = password
      tutorDto.email = email
      tutorDto.mobileNumber = phone
      this.tutorService.registerTutor(tutorDto).subscribe(res => {
        console.log(res);
        this._cacheService.set('phoneNumber', phone);
        this._cacheService.set('userType', 'ROLE_TUTOR');
        this.router.navigate(['verification']);
      }, (error) => {
        this.hasError = true;
        this.errorMessage = (error.error as BasicResponse).message
        this.shakable = 'shake';
        setTimeout(() => {
          this.shakable = 'fix';
        }, 500);
      });
    }


  }

  changeRoleSelection(id: number) {
    this.roleSelection = id;
  }
}

// let navigationExtras: NavigationExtras = {
//   queryParams: { 'section': 'formSelection','formType': '' + id},
// };
// this.router.navigate(['employee','forms'], navigationExtras);
