import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {shakeAnimation, slideInOutAnimation} from "../../../app.animations";
import {UserService} from "../../../Services/UserService";
import {JwtAuthenticationResponse} from "../../../Entity/Security/JwtAuthenticationResponse";
import {CacheService} from "ng2-cache-service";
import {Authority} from "../../../Entity/Security/Authority";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  animations: [shakeAnimation, slideInOutAnimation],
  providers: [UserService, CacheService]
})
export class SignInComponent implements OnInit {
  errorMessage = ''
  hasError = false;
  shakable = 'fix'
  spinner = false

  public mask = [ /[0]/ , /[9]/ , /[0-3]/, /\d/, '-' , /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]

  constructor(private router: Router, private activeRouter: ActivatedRoute, private userService: UserService, private cacheService: CacheService) { }

  ngOnInit() {

  }


  onSignIn(empho: string, pass: string) {
    this.spinner = true
    this.userService.login(empho, pass).subscribe(res => {
      console.log(res);
      let jwtRes = res as JwtAuthenticationResponse;
      this.cacheService.set('userToken', jwtRes.token)
      this.getAuthenticatedUser(jwtRes.authorities);
    },
    error => {
      console.log('imaaaaaan');
      if (error.status === 401) {
        this.hasError = true;
        this.errorMessage = 'اطلاعات وارد شده صحیح نمی باشد'
        this.shakable = 'shake';
        setTimeout(() => {
          this.shakable = 'fix';
        }, 500);
        this.spinner = false
      }
    });
  }


  getAuthenticatedUser(authorities: Array<Authority>)
  {
    this.userService.getMyProfile().subscribe(user => {
      this.cacheService.set('user', user);
      for (let auth of authorities)
      {
        if (auth.authority.toString() === 'ROLE_STUDENT')
        {
          this.cacheService.set('userType' , 'ROLE_STUDENT')
          this.router.navigate(['/student/questionList']);
        }
        else if (auth.authority.toString() === 'ROLE_TUTOR')
        {
          this.cacheService.set('userType' , 'ROLE_TUTOR')
          this.router.navigate(['/tutor/questionList']);
        }
        else if (auth.authority.toString() === 'ROLE_EMPLOYEE')
        {
          this.cacheService.set('userType' , 'ROLE_EMPLOYEE')
          this.router.navigate(['/employee/user-list']);
        }
        else if (auth.authority.toString() === 'ROLE_ADMIN')
        {
          this.cacheService.set('userType' , 'ROLE_ADMIN')
          this.router.navigate(['/employee/user-list']);
        }
      }
      this.spinner = false
    })
  }


  goToSignUp() {
    this.router.navigate(['signUp']);
  }

}
