import { Component, OnInit } from '@angular/core';
import {animate, state, style, transition, trigger} from "@angular/animations";
import {Router} from "@angular/router";
import {shakeAnimation, slideInOutAnimation} from "../../../app.animations";
import {UserService} from "../../../Services/UserService";
import {BasicResponse} from "../../../Entity/BasicResponse";
import {CacheService} from "ng2-cache-service";

@Component({
  selector: 'app-verification-page',
  templateUrl: './verification-page.component.html',
  styleUrls: ['./verification-page.component.scss'],
  animations: [slideInOutAnimation, shakeAnimation],
  providers: [UserService, CacheService]
})

export class VerificationPageComponent implements OnInit {
  hasError = false
  shakable = 'fix';
  errorMessage = '';
  phone = ''

  public mask = [ /[0]/ , /[9]/ , /[0-3]/, /\d/ , /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]

  public verificationMask = [  /\d/, /\d/, /\d/, /\d/, /\d/, /\d/]

  constructor(private cacheService: CacheService, private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.phone = this.cacheService.get('phoneNumber');
  }

  onVerification(verCode: string)
  {
    this.userService.registerVerification(verCode).subscribe(res => {
       let bres = res as BasicResponse;
       if (bres.success)
       {
         this.cacheService.set('userToken', bres.message)
         this.getAuthenticatedUser();
         if (this.cacheService.get('userType') === 'ROLE_STUDENT')
         {
            this.router.navigate(['/signUp' , 'gradeSelection'])
         }
         else if (this.cacheService.get('userType') === 'ROLE_TUTOR')
         {
            this.router.navigate(['/signUp' , 'tutorDetails'])
         }
       }

    },
    error =>{
      console.log(error)
      this.hasError = true
      this.errorMessage = (error.error as BasicResponse).message
      this.shakable = 'shake';
      setTimeout(() => {
        this.shakable = 'fix';
      }, 500);
    })
  }

  getAuthenticatedUser()
  {
    this.userService.getMyProfile().subscribe(user => {
      this.cacheService.set('user', user);
    })
  }


  onResendVerific()
  {
    console.log('imaaaaaaaaaaaa')
  }

}
