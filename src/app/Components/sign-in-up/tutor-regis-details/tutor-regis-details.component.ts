import { Component, OnInit } from '@angular/core';
import {shakeAnimation, slideInOutAnimation} from "../../../app.animations";
import {FileService} from "../../../Services/FileService";
import {AccessType, UploadedFile} from "../../../Entity/UploadedFile";
import {AppContextUrl, FileContextUrl} from "../../../app.globals";
import {UserService} from "../../../Services/UserService";
import {TutorService} from "../../../Services/TutorService";
import {Tutor} from "../../../Entity/User/Tutor";
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";
import {BasicResponse} from "../../../Entity/BasicResponse";
import {Router} from "@angular/router";

@Component({
  selector: 'app-tutor-regis-details',
  templateUrl: './tutor-regis-details.component.html',
  styleUrls: ['./tutor-regis-details.component.scss'],
  animations: [shakeAnimation, slideInOutAnimation],
  providers: [UserService, FileService, TutorService]
})
export class TutorRegisDetailsComponent implements OnInit {
  tutor: Tutor;
  avatar: UploadedFile;
  baseUrl = FileContextUrl

  spinner = false

  constructor(private router: Router, private tutorService: TutorService, private userService: UserService,private fileService: FileService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.loadUser();
  }

  loadUser(){
    this.userService.getMyProfile().subscribe(res => {
      console.log(res)
      this.tutor = res as Tutor;
      this.tutor.topics = [];

    })
  }

  onChangeTutorGender(event) {
    this.tutor.gender = event.value
  }
  onChangeTutorTopic(event)
  {
    console.log(event.value)
    this.tutor.topics = event.value
  }

  onPickProfile(event){
    this.fileService.uploadFile(event.target.files[0], AccessType[AccessType.Public] , '').subscribe((response) => {
      if (response['success'] == true) {
        this.avatar = new UploadedFile();
        this.avatar.id = response['message'];
      }
    })
  }
  onEdit(fname, lname, biography)
  {

      if (fname != null)
        this.tutor.firstName = fname;
      if (lname != null)
        this.tutor.lastName = lname;
      if (biography != null)
        this.tutor.biography = biography
      if (this.avatar != null)
        this.tutor.profileImage = this.avatar;

      this.saveTutor()



  }

  saveTutor()
  {
    this.spinner = true
    this.tutorService.editTutor(this.tutor.id,this.tutor).subscribe(res => {
      if ((res as BasicResponse).success)
      {
        this.openSnackBar('تغییرات با موفقیت ذخیره شد.' , 'success');
        this.router.navigate(['/tutor' , 'questionList'])
      }
      else
      {
        this.openSnackBar((res as BasicResponse).message , 'error');
      }
      this.spinner = false;
    })
  }


  openSnackBar(message: string, panelClass: string) {
    var conf = new MatSnackBarConfig();
    conf.duration = 8000;
    conf.panelClass = panelClass;
    this.snackBar.open(message, '', conf);
  }



}
