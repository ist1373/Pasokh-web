import { BrowserModule } from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { MainNavbarComponent } from './Components/main-navbar/main-navbar.component';
import {RouterModule,Routes} from "@angular/router";
import {appRoutes} from "./app.routes";
import { LandingPageComponent } from './Components/landing-page/landing-page.component';
import { FooterComponent } from './Components/footer/footer.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TextMaskModule} from "angular2-text-mask";
import {SignInComponent} from "./Components/sign-in-up/sign-in/sign-in.component";
import {VerificationPageComponent} from "./Components/sign-in-up/verification-page/verification-page.component";
import { SignUpComponent } from './Components/sign-in-up/sign-up/sign-up.component';
import {HTTP_INTERCEPTORS, HttpClientModule,HttpClient} from "@angular/common/http";
import { AboutUsComponent } from './Components/about-us/about-us.component';
import { ContactUsComponent } from './Components/contact-us/contact-us.component';
import {FormsModule} from "@angular/forms";
import {JalaliPipePipe} from "./Pipe/jalali-pipe.pipe";
import {PersainNumberPipe} from "./Pipe/persain-number.pipe";
import { PoliciesComponent } from './Components/policies/policies.component';
import {CacheService, LocalStorageServiceModule} from "ng2-cache-service";
import { ForgotPassComponent } from './Components/sign-in-up/forgot-pass/forgot-pass.component';
import { GradeSelComponent } from './Components/sign-in-up/grade-sel/grade-sel.component';
import { TutorRegisDetailsComponent } from './Components/sign-in-up/tutor-regis-details/tutor-regis-details.component';
import {MatRadioModule, MatSliderModule, MatSnackBarModule} from '@angular/material';
import { StudentNavbarComponent } from './Components/student-portal/student-navbar/student-navbar.component';
import { QuestionListComponent } from './Components/student-portal/question-list/question-list.component';
import {MatButtonModule} from '@angular/material/button';
import {MatTooltipModule} from '@angular/material/tooltip';
import {StarRatingModule} from "angular-star-rating";
import { AskQuestionComponent } from './Components/student-portal/ask-question/ask-question.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { TimeLineComponent } from './Components/student-portal/time-line/time-line.component';
import {VerticalTimelineModule} from "angular-vertical-timeline";
import {AddHeaderInterceptor} from "./Services/AddHeaderInterceptor";
import {EnumPipe} from "./Pipe/enum.pipe";
import { StudentProfileComponent } from './Components/student-portal/student-profile/student-profile.component';
import {SelectModule} from "ng-select";
import { PurchasePageComponent } from './Components/student-portal/purchase-page/purchase-page.component';
import { ChatRoomComponent } from './Components/chat-room/chat-room.component';
import {ScrollEventModule} from "ngx-scroll-event";
import {StompConfig, StompService} from "@stomp/ng2-stompjs";
import {stompConfig} from "./WebSocket.config";
import { TutorQuestionListComponent } from './Components/tutor-portal/tutor-question-list/tutor-question-list.component';
import { TutorNavbarComponent } from './Components/tutor-portal/tutor-navbar/tutor-navbar.component';
import { TutorChatroomsComponent } from './Components/tutor-portal/tutor-chatrooms/tutor-chatrooms.component';
import { BlogCatsComponent } from './Components/blog-portal/blog-cats/blog-cats.component';
import { TutorProfileComponent } from './Components/tutor-portal/tutor-profile/tutor-profile.component';
import { FinancialAccountComponent } from './Components/tutor-portal/financial-panel/financial-account/financial-account.component';
import { FinancialNavComponent } from './Components/tutor-portal/financial-panel/financial-nav/financial-nav.component';
import { TutorTransactionsComponent } from './Components/tutor-portal/financial-panel/tutor-transactions/tutor-transactions.component';
import { TutorPaymentsComponent } from './Components/tutor-portal/financial-panel/tutor-payments/tutor-payments.component';
import { PostListComponent } from './Components/blog-portal/post-list/post-list.component';
import { BlogDetailsComponent } from './Components/blog-portal/blog-details/blog-details.component';
import { PurchaseHistoryComponent } from './Components/student-portal/purchase-history/purchase-history.component';
import { EmployeeNavbarComponent } from './Components/employee-portal/employee-navbar/employee-navbar.component';
import { UserListComponent } from './Components/employee-portal/user-list/user-list.component';
import { StudentDetailsComponent } from './Components/employee-portal/student-details/student-details.component';
import { TutorDetailsComponent } from './Components/employee-portal/tutor-details/tutor-details.component';
import { EmployeeDetailsComponent } from './Components/employee-portal/employee-details/employee-details.component';
import { AddPersonelComponent } from './Components/employee-portal/add-personel/add-personel.component';
import { ComplaintListComponent } from './Components/employee-portal/complaint-list/complaint-list.component';
import { PurchaseManagementComponent } from './Components/employee-portal/purchase-management/purchase-management.component';
import {CacheMemoryStorage} from "ng2-cache-service/dist/src/services/storage/memory/cache-memory.service";
import {CacheStorageAbstract} from "ng2-cache-service/dist/src/services/storage/cache-storage-abstract.service";
import {CacheLocalStorage} from "ng2-cache-service/dist/src/services/storage/local-storage/cache-local-storage.service";
import {MomentModule} from "angular2-moment";

@NgModule({
  declarations: [
    AppComponent,
    MainNavbarComponent,
    LandingPageComponent,
    FooterComponent,
    SignInComponent,
    VerificationPageComponent,
    SignUpComponent,
    AboutUsComponent,
    ContactUsComponent,
    PersainNumberPipe,
    JalaliPipePipe,
    EnumPipe,
    PoliciesComponent,
    ForgotPassComponent,
    GradeSelComponent,
    TutorRegisDetailsComponent,
    StudentNavbarComponent,
    QuestionListComponent,
    AskQuestionComponent,
    TimeLineComponent,
    StudentProfileComponent,
    PurchasePageComponent,
    ChatRoomComponent,
    TutorQuestionListComponent,
    TutorNavbarComponent,
    TutorChatroomsComponent,
    BlogCatsComponent,
    TutorProfileComponent,
    FinancialAccountComponent,
    FinancialNavComponent,
    TutorTransactionsComponent,
    TutorPaymentsComponent,
    PostListComponent,
    BlogDetailsComponent,
    PurchaseHistoryComponent,
    EmployeeNavbarComponent,
    UserListComponent,
    StudentDetailsComponent,
    TutorDetailsComponent,
    EmployeeDetailsComponent,
    AddPersonelComponent,
    ComplaintListComponent,
    PurchaseManagementComponent
  ],
  imports: [
    BrowserModule,
    TextMaskModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpModule,
    LocalStorageServiceModule,
    MatRadioModule,
    MatButtonModule,
    MatSnackBarModule,
    MatTooltipModule,
    VerticalTimelineModule,
    MatButtonToggleModule,
    MatSliderModule,
    SelectModule,
    ScrollEventModule,
    MomentModule,
    StarRatingModule.forRoot(),
    MDBBootstrapModule.forRoot(),
    RouterModule.forRoot(appRoutes),
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AddHeaderInterceptor,
    multi: true,
  },
    StompService,
    {
      provide: StompConfig,
      useValue: stompConfig
    },
    CacheService,
    {provide: CacheStorageAbstract, useClass: CacheLocalStorage},
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  bootstrap: [AppComponent]
})
export class AppModule { }
// {provide: CacheStorageAbstract, useClass: CacheMemoryStorage},
