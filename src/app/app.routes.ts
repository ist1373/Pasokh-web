import {Routes} from '@angular/router';
import {AppComponent} from "./app.component";
import {LandingPageComponent} from "./Components/landing-page/landing-page.component";
import {VerificationPageComponent} from "./Components/sign-in-up/verification-page/verification-page.component";
import {SignInComponent} from "./Components/sign-in-up/sign-in/sign-in.component";
import {SignUpComponent} from "./Components/sign-in-up/sign-up/sign-up.component";
import {AboutUsComponent} from "./Components/about-us/about-us.component";
import {ContactUsComponent} from "./Components/contact-us/contact-us.component";
import {PoliciesComponent} from "./Components/policies/policies.component";
import {ForgotPassComponent} from "./Components/sign-in-up/forgot-pass/forgot-pass.component";
import {GradeSelComponent} from "./Components/sign-in-up/grade-sel/grade-sel.component";
import {TutorRegisDetailsComponent} from "./Components/sign-in-up/tutor-regis-details/tutor-regis-details.component";
import {QuestionListComponent} from "./Components/student-portal/question-list/question-list.component";
import {AskQuestionComponent} from "./Components/student-portal/ask-question/ask-question.component";
import {TimeLineComponent} from "./Components/student-portal/time-line/time-line.component";
import {StudentProfileComponent} from "./Components/student-portal/student-profile/student-profile.component";
import {PurchasePageComponent} from "./Components/student-portal/purchase-page/purchase-page.component";
import {ChatRoomComponent} from "./Components/chat-room/chat-room.component";
import {TutorQuestionListComponent} from "./Components/tutor-portal/tutor-question-list/tutor-question-list.component";
import {TutorChatroomsComponent} from "./Components/tutor-portal/tutor-chatrooms/tutor-chatrooms.component";
import {BlogCatsComponent} from "./Components/blog-portal/blog-cats/blog-cats.component";
import {TutorProfileComponent} from "./Components/tutor-portal/tutor-profile/tutor-profile.component";
import {FinancialAccountComponent} from "./Components/tutor-portal/financial-panel/financial-account/financial-account.component";
import {TutorTransactionsComponent} from "./Components/tutor-portal/financial-panel/tutor-transactions/tutor-transactions.component";
import {TutorPaymentsComponent} from "./Components/tutor-portal/financial-panel/tutor-payments/tutor-payments.component";
import {PostListComponent} from "./Components/blog-portal/post-list/post-list.component";
import {BlogDetailsComponent} from "./Components/blog-portal/blog-details/blog-details.component";
import {PurchaseHistoryComponent} from "./Components/student-portal/purchase-history/purchase-history.component";
import {UserListComponent} from "./Components/employee-portal/user-list/user-list.component";
import {StudentDetailsComponent} from "./Components/employee-portal/student-details/student-details.component";
import {TutorDetailsComponent} from "./Components/employee-portal/tutor-details/tutor-details.component";
import {EmployeeDetailsComponent} from "./Components/employee-portal/employee-details/employee-details.component";
import {AdminService} from "./Services/AdminService";
import {AddPersonelComponent} from "./Components/employee-portal/add-personel/add-personel.component";
import {ComplaintListComponent} from "./Components/employee-portal/complaint-list/complaint-list.component";
import {PurchaseManagementComponent} from "./Components/employee-portal/purchase-management/purchase-management.component";

/**
 * Created by iman on 6/13/2017.
 */

export const appRoutes: Routes = [

  { path: '', component: LandingPageComponent, pathMatch: 'full' }, //TODO add gurds to routes

  { path: 'tutor/questionList', component: TutorQuestionListComponent },
  { path: 'tutor/timeLine', component: TimeLineComponent },
  { path: 'tutor/chatrooms', component: TutorChatroomsComponent },
  { path: 'blog/cats', component: BlogCatsComponent },
  { path: 'blog/posts/:id', component: BlogDetailsComponent },
  { path: 'blog/posts', component: PostListComponent },
  { path: 'tutor/chatRoom/:roomUid', component: ChatRoomComponent },
  { path: 'tutor/profile', component: TutorProfileComponent },
  { path: 'tutor/financial-panel/account', component: FinancialAccountComponent },
  { path: 'tutor/financial-panel/transactions', component: TutorTransactionsComponent },
  { path: 'tutor/financial-panel/payments', component: TutorPaymentsComponent },

  { path: 'employee/user-list', component: UserListComponent },
  { path: 'employee/studentDetails/:id', component: StudentDetailsComponent },
  { path: 'employee/tutorDetails/:id', component: TutorDetailsComponent },
  { path: 'employee/employeeDetails/:id', component: EmployeeDetailsComponent },
  { path: 'employee/addPersonnel', component: AddPersonelComponent },
  { path: 'employee/purchaseManagement', component: PurchaseManagementComponent },
  { path: 'employee/complaintList', component: ComplaintListComponent },
  { path: 'employee/chatRoom/:roomUid', component: ChatRoomComponent },

  { path: 'employee/blog/cats', component: BlogCatsComponent },
  { path: 'employee/blog/posts/new', component: BlogDetailsComponent },
  { path: 'employee/blog/posts/:id', component: BlogDetailsComponent },
  { path: 'employee/blog/posts', component: PostListComponent },


  { path: 'student/questionList', component: QuestionListComponent },
  { path: 'student/askQuestion', component: AskQuestionComponent },
  { path: 'student/timeLine', component: TimeLineComponent },
  { path: 'student/profile', component: StudentProfileComponent },
  { path: 'student/purchasePage', component: PurchasePageComponent },
  { path: 'student/purchaseHistory', component: PurchaseHistoryComponent },
  { path: 'student/chatRoom/:roomUid', component: ChatRoomComponent },
  { path: 'signIn', component: SignInComponent },
  { path: 'signUp/gradeSelection', component: GradeSelComponent },
  { path: 'signUp/tutorDetails', component: TutorRegisDetailsComponent },
  { path: 'signUp', component: SignUpComponent },
  { path: 'forgotPass', component: ForgotPassComponent },
  { path: 'verification', component: VerificationPageComponent },
  { path: 'about-us', component: AboutUsComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'policies', component: PoliciesComponent },
  { path: '**', redirectTo: '/404'}
];

// { path: '404', component: NotFoundPageComponent},
// { path: 'AccessDenied', component: AccessDeniedPageComponent},
